
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/test/tools/cloud-rendering/140c/src/nbody.cpp" "CMakeFiles/nbody.dir/src/nbody.cpp.o" "gcc" "CMakeFiles/nbody.dir/src/nbody.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/test/tools/cloud-rendering/140c/build/CMakeFiles/nbodycuda.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
