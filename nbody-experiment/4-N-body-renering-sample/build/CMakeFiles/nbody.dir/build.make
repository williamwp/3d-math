# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.25

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /snap/cmake/1204/bin/cmake

# The command to remove a file.
RM = /snap/cmake/1204/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/test/tools/cloud-rendering/140c

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/test/tools/cloud-rendering/140c/build

# Include any dependencies generated for this target.
include CMakeFiles/nbody.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include CMakeFiles/nbody.dir/compiler_depend.make

# Include the progress variables for this target.
include CMakeFiles/nbody.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/nbody.dir/flags.make

CMakeFiles/nbody.dir/src/nbody.cpp.o: CMakeFiles/nbody.dir/flags.make
CMakeFiles/nbody.dir/src/nbody.cpp.o: /home/test/tools/cloud-rendering/140c/src/nbody.cpp
CMakeFiles/nbody.dir/src/nbody.cpp.o: CMakeFiles/nbody.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/test/tools/cloud-rendering/140c/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/nbody.dir/src/nbody.cpp.o"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT CMakeFiles/nbody.dir/src/nbody.cpp.o -MF CMakeFiles/nbody.dir/src/nbody.cpp.o.d -o CMakeFiles/nbody.dir/src/nbody.cpp.o -c /home/test/tools/cloud-rendering/140c/src/nbody.cpp

CMakeFiles/nbody.dir/src/nbody.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/nbody.dir/src/nbody.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/test/tools/cloud-rendering/140c/src/nbody.cpp > CMakeFiles/nbody.dir/src/nbody.cpp.i

CMakeFiles/nbody.dir/src/nbody.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/nbody.dir/src/nbody.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/test/tools/cloud-rendering/140c/src/nbody.cpp -o CMakeFiles/nbody.dir/src/nbody.cpp.s

# Object files for target nbody
nbody_OBJECTS = \
"CMakeFiles/nbody.dir/src/nbody.cpp.o"

# External object files for target nbody
nbody_EXTERNAL_OBJECTS =

CMakeFiles/nbody.dir/cmake_device_link.o: CMakeFiles/nbody.dir/src/nbody.cpp.o
CMakeFiles/nbody.dir/cmake_device_link.o: CMakeFiles/nbody.dir/build.make
CMakeFiles/nbody.dir/cmake_device_link.o: libnbodycuda.a
CMakeFiles/nbody.dir/cmake_device_link.o: CMakeFiles/nbody.dir/dlink.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/test/tools/cloud-rendering/140c/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CUDA device code CMakeFiles/nbody.dir/cmake_device_link.o"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/nbody.dir/dlink.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/nbody.dir/build: CMakeFiles/nbody.dir/cmake_device_link.o
.PHONY : CMakeFiles/nbody.dir/build

# Object files for target nbody
nbody_OBJECTS = \
"CMakeFiles/nbody.dir/src/nbody.cpp.o"

# External object files for target nbody
nbody_EXTERNAL_OBJECTS =

nbody: CMakeFiles/nbody.dir/src/nbody.cpp.o
nbody: CMakeFiles/nbody.dir/build.make
nbody: libnbodycuda.a
nbody: CMakeFiles/nbody.dir/cmake_device_link.o
nbody: CMakeFiles/nbody.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/test/tools/cloud-rendering/140c/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX executable nbody"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/nbody.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/nbody.dir/build: nbody
.PHONY : CMakeFiles/nbody.dir/build

CMakeFiles/nbody.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/nbody.dir/cmake_clean.cmake
.PHONY : CMakeFiles/nbody.dir/clean

CMakeFiles/nbody.dir/depend:
	cd /home/test/tools/cloud-rendering/140c/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/test/tools/cloud-rendering/140c /home/test/tools/cloud-rendering/140c /home/test/tools/cloud-rendering/140c/build /home/test/tools/cloud-rendering/140c/build /home/test/tools/cloud-rendering/140c/build/CMakeFiles/nbody.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/nbody.dir/depend

