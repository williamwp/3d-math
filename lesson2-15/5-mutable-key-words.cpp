#include<iostream>

using namespace std;

class A
{
public:
    A(int a1)
    {
        int a = a1;

    }

    void print() const
    {
        a = 200;
        cout << "a =  " << a << endl;
    }


private:
    mutable int a;  

};


int main()
{
    A a(111);
    a.print();
// mutable key word can change the value of const key word in the function
    return 0;
}
