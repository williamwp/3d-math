//STL算法 -- 删除算法（2） 
//remove_copy()
//remove_copy_if()
//预定义的函数适配器  bind1st(op, value)   bind2nd(op, value)   not1(op)   not2(op)   
// mem_fun_ref(op)    mem_fun(op)   ptr_fun(op)
 
//预定义的函数对象
//negate<type>()  plus<type>()  minus<type>()  multiplies<type>() 
//divides<type>()  modulus<type>()  equal_to<type>()   not_equal_to<type>()  less<type>()  greater<type>()
// less_equal<type>()  greater_equal<type>()  logical_not<type>()  logical_and<type>()  logical_or<type>()
#include<iostream>
#include<list>
#include<vector>
#include<algorithm>
#include<set>
#include<iterator>

using namespace std;

int main()
{
	list<int> ilist;
	
	
	for(int i=0; i <= 6; ++i)
	    ilist.push_back(i);
	    
	for(int j=2; j <= 9; ++j)
	    ilist.push_back(j);
	
	cout << " list<int>::iterator iter1 = ilist.begin() "<< endl;
	for(list<int>::iterator iter1 = ilist.begin(); iter1 != ilist.end(); ++iter1)
	    cout << "  " << *iter1;
	    cout << endl;
	cout << "ilist.size() = "<< ilist.size() << endl; 
	
	
	multiset<int> iset;
	remove_copy_if(ilist.begin(),ilist.end(), 
	               inserter(iset, iset.end()),
				   bind2nd(less<int>(), 4));
	cout<<endl;
	cout<<endl;
	
	
	//multiset是二叉树，红黑树 
	cout << " remove_copy_if,  inserter(iset, iset.end()), bind2nd(less<int>(), 4))"<< endl;
	for(multiset<int>::iterator iter2 = iset.begin(); iter2 != iset.end(); ++iter2)
	    cout << "  " << *iter2;
	    cout << endl;
	cout << "ilist.size() = "<< iset.size() << endl; 
	cout<<endl;
	cout<<endl;
	
	
	
	cout << "remove_copy(ilist.begin(), ilist.end(),  ostream_iterator<int>(cout, \" \"), 3)"<< endl;
	remove_copy(ilist.begin(), ilist.end(),
	            ostream_iterator<int>(cout, " "), 3);
	cout<<endl;
	cout<<endl;			
				
	
	cout << "remove_copy_if(ilist.begin(), ilist.end(), ostream_iterator<int>(cout, \" \"), bind2nd(greater<int>(), 4))  " << endl;
	remove_copy_if(ilist.begin(), ilist.end(),
	               ostream_iterator<int>(cout, " "), bind2nd(greater<int>(), 4));
	cout << endl;
	cout << endl;
	return 0; 
}

