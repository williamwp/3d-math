#include <iostream>
#include <stdexcept>
using namespace std;

class A
{
public:
    A()
	{
		a = 10;
	}
	int a;	
	
};

class MyException : public exception
{
public:
   // MyException(const char *p) : exception(p)
    //{}
		
//	char *p1;
	
}; 

int main()
{
	try
	{
		MyException e;
		throw std::exception(e);
	//	throw MyException("my exception \n");
		
	}
	catch(const exception& e)
	{
		cout << " self-defined standard C++ exception : " << e.what() << endl;
	}
	
//******************************************************** 
	/*
	try
	{
		throw A();
	}
	catch(A a)
	{
		cout << " catch the exception:" << a.a << endl;
		
	}
	*/
	
//******************************************************** 
	/*
	try
	{
		for(int i=0; i<1000;++i)
		{
			 new int[100000];
		}
	}
	catch (const exception& e)
	{
		cout << " standard c++ exception: " << e.what() << endl;
		//这个没有显示出 bad allocation异常提示 
	}
	*/


//******************************************************** 
    /*
--------------------------------
Process exited after 0.4911 seconds with return value 0
请按任意键继续. . .
	*/

//******************************************************** 

	
	return 0;
}
