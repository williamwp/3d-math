//在windows上编写的文件，上传至Linux上，vim编辑时乱码。
//原因是windows默认使用gbk编码，在Linux上，使用iconv命令可以转码：
//#iconv -f GBK -t UTF-8 test.txt -o test2.txt再次vim编辑时OK。
#include <iostream>

using namespace std;

class A
{
public:
    A(int a1)
	{
	    a = a1;	//此处注意，不要加int，不然三个值都是0 
	    /*
a = 0
called void printA()
a = 0
called void printA()
a = 0
called void printA()
		*/
	    
	    cout << "调用构造函数 \n\n" << endl; 
	}	
	//固定写法 
	A(const A &a1) //内部应该是 const 类名 & 
	{
	    a = a1.a;	//调用类中拷贝构造函数对象的值 
	    cout << "调用拷贝构造函数 \n\n" << endl; 
	}
	
	void printA()
	{
		cout << "a = " << a << endl;
		cout << "called void printA() " << endl;
		
	}
	
	
private:
	int a;
};

int main()
{
	A a(222); //调用有参构造函数
	A b(a);   //定义的时候初始化，调用有参拷贝构造函数
	A c=a;    //定义的时候用对象初始化，调用有参拷贝构造函数
    
	a.printA(); 
    b.printA();
    c.printA();
	return 0;
}
