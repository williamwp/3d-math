iconv -f GBK -t UTF-8  6-const-object.cpp -o 6-const-object1.cpp 

#include <iostream>

using namespace std;

class A
{
public:
	A(int a1)
	{
		int a = a1;
	}
	void printA()  //普通成员函数 
	{
		a = 20; 
		cout << " called	void printA()  " << endl;
		cout << "a = " << a << endl;
	}
	void printB() const //常成员函数 
	//如果是const void printB()，就会报错 
	//passing 'const A' as 'this' argument of 'const void A::printB()' discards qualifiers [-fpermissive] 
	//将“const A”作为“const void A：：printB（）”的“this”参数传递将丢弃限定符[-fpermissive] 
	{
//		a =200; // 常成员函数不可以修改变量的值 
		cout << " called const void printB() " << endl;
		cout << "a = " << a << endl;
	}
	
private:
	int a;
};


int main()
{
//	A a(11);
    A const a1(1);
	
//	a.printA();
	a1.printB();
	
	return 0; 
	
}
 
