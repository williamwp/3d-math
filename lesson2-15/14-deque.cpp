#include<iostream>
#include<deque>
/*
顺序容器 STL deque 类
***deque是一个动态数组
***deque与vector非常相似
***deque可以在数组开头和末尾插入和删除数据
deque和vector的区别是----vector只能push_back，只能pop_back 

*/
using namespace std;

int main()
{
	
	deque<int> a;
	//add the data at the back of the deque 
	a.push_back(3);
	a.push_back(4);
	a.push_back(5);
	
	//add the data in front of the deque
	a.push_front(2);
	a.push_front(1);
	a.push_front(0);
	
	
	// show the values in the deque
	//for(size_t i = 0; i < a.size(); ++i)  --> also OK
	for(int i = 0; i < a.size(); ++i)
	{
		cout << " show the values in the deque: " << a[i] << endl;
	}
	cout<<endl;
	
	//cout directly
	cout << " a[0] = " << a[0] << endl;
	cout << " a[1] = " << a[1] << endl;
	
	
	cout<<endl;
	
	//delete data directly
	a.pop_back();
	a.pop_front();
	for(int i = 0; i < a.size(); ++i)
	{
		cout << " show the values in the deque: " << a[i] << endl;
	}
	
	
	cout<<endl;
	// deque iterator
	deque<int>::iterator iter;
	for(iter = a.begin(); iter != a.end(); ++iter)
	{
		size_t nOffset = distance(a.begin(), iter);
		cout << " deque<int>::iterator iter; " << "a[" << nOffset << "]";
		cout << " is: " <<  *iter << endl;
	}
	
	
	return 0;
}
