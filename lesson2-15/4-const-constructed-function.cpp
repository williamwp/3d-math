#include<iostream>

using namespace std;
/*

test@test-C9Z490-PGW:~/tools/3d-math/lesson2-15$ g++ 4-const-constructed-function.cpp
4-const-constructed-function.cpp: In constructor ‘A::A(int)’:
4-const-constructed-function.cpp:9:13: error: declaration of ‘int a1’ shadows a parameter
         int a1;
             ^~
4-const-constructed-function.cpp: In function ‘int main()’:
4-const-constructed-function.cpp:27:11: error: ‘A::A(int)’ is private within this context
     A a(10);
           ^
4-const-constructed-function.cpp:7:5: note: declared private here
     A(int a1):a(a1)
     ^

*/

class A
{ 
public:
    A(int a1):a(a1)
    {
//        const int a1;
        cout << " called construction function A(const int a1):a(a1) " << endl;
    }
 
    void print()
    { 
        cout << " called construction function A(const int a1):a(a1) " << endl;
        cout << "a is  " << a << endl;
    }

private:
    const int a;

};



int main()
{
    A  a(11);
    a.print();


    return 0;
}
