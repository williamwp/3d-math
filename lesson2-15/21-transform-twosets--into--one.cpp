#include<iostream>
#include<vector>
#include<list>
#include<algorithm>
#include<iterator>


using namespace std;

int main()
{
	vector<int> ivec;
	list<int> ilist;
	
	
	for(int i=0; i<=9;i++)
	ivec.push_back(i);
	
	for(vector<int>::iterator iter = ivec.begin(); iter!=ivec.end(); iter++)
	{
		cout <<" vector<int>iterator iter = ivec.begin() " << *iter << endl;
	}
	cout << endl;
	cout << endl;
	
	cout<< " transform(ivec.begin(), ivec.end(), ivec.begin(), " <<
		 " ivec.begin(), multiplies<int>() ); " << endl; 
	transform(ivec.begin(), ivec.end(),
	        ivec.begin(),
			back_inserter(ilist),
			plus<int>() );
	//第二个容器用一个逆向迭代器 ivec.rbegin(),
	//把向量里的第一个数和最后一个数相加，第二个数和倒数第二个数相加 
	
	cout << endl;
	cout << endl;
	cout << " 第二个容器用一个逆向迭代器 ivec.rbegin(), " << endl;
	for(list<int>::iterator iter = ilist.begin(); iter!=ilist.end(); ++iter)
	{
		cout <<" list<int>::iterator iter = ilist.begin() " << *iter << endl;
	}
	cout << endl;
	cout << endl;
	
	
	//向量里的数据，减去列表里的数据
	//将结果输出到 cout中 
	cout <<" 向量里的数据，减去列表里的数据, 将结果输出到 cout中 " << endl;
	transform(ivec.begin(), ivec.end(),
	          ilist.begin(),
			  ostream_iterator<int>(cout, " "),
			  minus<int>() );
	return 0;
	
}
