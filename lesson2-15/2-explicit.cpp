#include <iostream>

using namespace std;

class A
{
public:
/*
2-explicit.cpp: In function ‘int main()’:
2-explicit.cpp:32:11: error: conversion from ‘int’ to non-scalar type ‘A’ requested
     A a = 12; // the reason why object a has value is that the constructed function can be called explicit

*/
//    explicit A(int _a):a(_a)

    A(int _a):a(_a)
    {
        cout << " called A(int _a):a(_a) " << endl;
        this -> a = a; 
        // this points to variable a
    }
    void print()
    {

        cout << a << " \n\n " << endl;
    }

private:
    int a;

};



int main()
{
    A a0(131); // called the construction function
    a0.print();
    cout << "explicit A(int _a):a(_a) \n\n  " << endl;

    A a = 12; // the reason why object a has value is that the constructed function can be called explicit
    // the g++ compiler converts the integer to the value of object A a
    a.print();




    return 0;
}
