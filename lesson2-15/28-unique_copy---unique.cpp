//STL算法 -- 删除算法 
//unique(b,e)是把连续的数字删除。       unique_copy(b1, e1, b2)
//unique(b,e,p)     unique_copy_if(b1, e1, b2, p)
//注意！！！ 应该有一个unique_if和unique_copy_if() 但是没有 
#include<iostream>
#include<vector>
#include<list>
#include<algorithm>
#include<iterator>

using namespace std;

bool differenceOne(int elem1, int elem2)
{
	return elem1 -1 == elem2 || elem1 +1 == elem2;
}

int main()
{
	
	int source[] = {1,1,2,2,2,3,1,1,1,1,4,4,4,5,5,6,6,7,7,7,8,8,8,9,9,1010};
	int sourceNum = sizeof(source)/sizeof(source[0]);
	
	cout << "int sourceNum = sizeof(source)/sizeof(source[0]) is " << sourceNum << endl;
	list<int> ilist;
	copy(source, source+sourceNum, back_inserter(ilist));
	
	
	
	cout << "copy(source, source+sourceNum, back_inserter(ilist)) " << endl;
	for(list<int>::iterator iter1 = ilist.begin(); iter1 != ilist.end();  ++iter1)
	    cout << "  " << *iter1;
	cout << endl;
	cout << endl;
	cout << endl;
	
	
	

	list<int>::iterator pos;
	pos = unique(ilist.begin(), ilist.end());
	
	
	cout << endl; 
	cout << "pos = unique(ilist.begin(), ilist.end()) " << endl;
	for(list<int>::iterator iter2 = ilist.begin(); iter2 != ilist.end();  ++iter2)
	    cout << "  " << *iter2 ;
	cout << endl;
	cout << endl;
	cout << endl;
	
	
	
	cout << "copy(source, source+sourceNum, ilist.begin()) " << endl;
	copy(source, source+sourceNum, ilist.begin());
    for(list<int>::iterator iter3 = ilist.begin(); iter3 != ilist.end(); ++iter3)
        cout << "  " << *iter3;
	cout << endl;
	cout << endl;
	cout << endl;
	
	
	pos = unique(ilist.begin(), ilist.end(), greater<int>());
	//greater做谓词，用来比较两个数，如果前面的一个数字比后面的数字大，就把后面的数字删除。 
	cout << "pos = unique(ilist.begin(), ilist.end(), greater<int>()) " << endl;
	for(list<int>::iterator iter5 = ilist.begin(); iter5 != ilist.end(); ++iter5)
	    cout << "  " << *iter5 ;
	    
	    
	cout << endl;
	cout << endl;    
	copy(source, source+sourceNum, back_inserter(ilist));
	for(list<int>::iterator iter5 = ilist.begin(); iter5 != ilist.end(); ++iter5)
	    cout << " " << *iter5 ;
	cout << endl;
	cout << endl;
	cout << endl;
	
	
	cout << "unique_copy(ilist.begin(), ilist.end(), ostream_iterator<int>(cout, " ")) " << endl;
	unique_copy(ilist.begin(), ilist.end(), ostream_iterator<int>(cout, " "));
	cout << endl;
	cout << endl;
	
	
	cout << "unique_copy(ilist.begin(), ilist.end(), ostream_iterator<int>(cout, \" \"), differenceOne) " << endl;
	unique_copy(ilist.begin(), ilist.end(),
	            ostream_iterator<int>(cout, " "),
				differenceOne);
	//使用自己写的函数谓词 differenceOne
	cout << endl;
	cout << endl;
	
	
	return 0;
}

