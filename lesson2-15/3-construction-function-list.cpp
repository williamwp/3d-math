#include<iostream>

using namespace std;


class A
{
public:
/*
test@test-C9Z490-PGW:~/tools/3d-math/lesson2-15$ ./a.out 
the construction function A(int a, int b)  
print variable a and b  a = 1479569728  b = 32767


 
test@test-C9Z490-PGW:~/tools/3d-math/lesson2-15$ vim 3-construction-function-list.cpp
test@test-C9Z490-PGW:~/tools/3d-math/lesson2-15$ g++ 3-construction-function-list.cpp 
3-construction-function-list.cpp: In function ‘int main()’:
3-construction-function-list.cpp:35:16: error: cannot bind non-const lvalue reference of type ‘int&’ to an rvalue of type ‘int’
     A a(111,222);
                ^
3-construction-function-list.cpp:10:5: note:   initializing argument 1 of ‘A::A(int&, int&)’
     A(int &a, int &b)

*/
// if user doesn't define primary parameter list, then the values are in a mass

    A(int _a, int _b):a(_a),b(_b)
    {

      cout << "the construction function A(int a, int b)  " << endl;

    }
    

    A(float a1, float b1)
    {
        this -> a1 = a1;
        this -> b1 = b1;
    }
    
/*
also OK

A(int a11, int b11)
{
    a = a11;
    b = b11;
    cout << "  called A(int a11, int b11) " << endl;

}


*/ 
    void printInt()
    {
        cout << "print variable a and b  "<< "a = " << a << "  b = " << b <<  endl;
        cout << "\n\n " << endl;
    }


 
    void printFloat()
    {   
        cout << "print variable a1 and b1  "<< "a1 = " << a1 << "  b1 = " << b1 <<  endl;
        cout << "\n\n " << endl;
    } 

private:
    int a;
    int b;
    float a1;
    float b1;
};




int main()
{

    A a(111,222);
    a.printInt();

    A a1(1.2f, 1.3f);
    a1.printFloat();

    return 0;
}
