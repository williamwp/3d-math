#include<iostream>

using namespace std;


class A
{
public:
	A(int a1)
	{
		a = a1;
		cout << " 调用默认的构造函数 \n " << endl;
	}
	
	A operator +(const A &a1)
	{
		return A(a+a1.a);
		
	} 
	void printA()
	{
		cout << "called void printA(), a = "  << a << endl;
	}
	
	~A()
	{
		cout << " 调用析构函数 \n " << endl;
	}


private:	
	int a;
	
};


int main()
{
	A a(10);
	A b(20);
	
	A c=a+b;
	a.printA();
	b.printA();
	c.printA();
	
	
	
	
	
	
	
	
/*
运算符重载的格式
类型 operator 运算符(形参表)
{
    函数体; 
} 
*/	
	
/*	
	// 输出显示
	//  调用默认的构造函数
    //  调用析构函数 
	//第一种方式调用析构函数 
	A a; //创建对象的时候构造，销毁的时候析构函数 
	
	
	//第二种方式调用析构函数
	A *p = new A;
	delete p;
	
*/	 
	
	return 0;
}
