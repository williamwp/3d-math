#include<iostream>
#include<queue>
#include<list>
#include<deque> 
//！！！堆栈和队列都没有迭代器 
//STL queue队列, FIFO
//queue<int, deque<int> >  q, queue<int, list<int> >  q
//q.empty()  q.size()   q.front()队首   q.back()队尾   q.pop()   q.push(item)

using namespace std;
/*
原材料： 布，皮，纸 
        list,deque,vector
	鞋：布鞋，皮鞋，不能用纸做鞋
	 
*/


int main()
{
	queue<int, deque<int> >  a;
	//20	2	C:\Users\william\Desktop\C++\17-queue-FIFO.cpp
	//[Error] declaration does not declare anything [-fpermissive]
	queue<int, list<int> >  b;
	queue<int>  c;
	
	c.push(1);
	c.push(2);
	c.push(3);
	c.push(4);
	
	cout << " queue c.size is " << c.size() << endl; 
	cout << " queue c.front() is " << c.front() << endl;
	cout << " queue c.back() is "  << c.back() << endl;
	c.pop(); //first in, first out
	cout << "after c.pop(),  queue c.front() is "  << c.front()  << endl;
	cout << endl << endl << endl;
	
	
	cout << "start loop while" << endl;
	//while(c.size() != 0)
	while(c.empty() ==false )
	{
		cout << "queue  c.front() is " << c.front() << endl;
		c.pop();
	}
	
	cout << endl << endl << endl;
	if(c.empty() == true)  //if(c.empty()) 
	cout << "the queue is empty now. " << endl;
	
	
	return 0;
}
