//STL�㷨�������ֵ
// fill(b,e,v)   fill_n(b,n,v)
//generate(b,e,p)  generate_n(b,n,p)

#include<iostream>
#include<vector>
#include<list>
#include<algorithm>
#include<iterator>

using namespace std;

int main()
{
	list<string> slist;
	slist.push_back("hello");
	slist.push_back("hi ");
	slist.push_back("good morning ");
	
	fill(slist.begin(), slist.end(), "hao ");
	 
	cout << "list<int>::iterator iter = slist.begin()  " << endl;
	for(list<string>::iterator iter = slist.begin(); iter != slist.end(); ++iter)
	     cout << *iter << endl;
	cout << endl << endl;
	
	list<string> slist2;
	fill_n(back_inserter(slist2), 9, "hello");
	cout << "list<string>::iterator iter = slist2.begin() " << endl;
	for(list<string>::iterator iter = slist2.begin(); iter!=slist2.end(); ++iter)
	    cout << " " << *iter << endl;
	
	
	cout<< "fill_n(ostream_iterator<float>(cout, " "), 10, 7.7);  " << endl;
	fill_n(ostream_iterator<float>(cout, " "), 10, 7.7); 
	cout << endl;
	
	fill(slist2.begin(), slist2.end(),"aa");
	cout << "fill slist2"<< endl;
	for(list<string>::iterator iter2 = slist2.begin(); iter2!=slist2.end(); ++iter2)
	    cout << " " << *iter2 << endl;
	
	cout << " fill(++pos1, --pos2, \"pig\" ) " << endl;
	list<string>::iterator pos1, pos2;
	pos1 = slist2.begin();
	pos2 = slist2.end();
	cout << endl;
	cout << endl;
	
	
	fill(++pos1, --pos2, "pig");
	cout << "fill slist2, ++pos1, --pos2, pig "<< endl;
	for(list<string>::iterator iter3 = slist2.begin(); iter3!=slist2.end(); ++iter3)
	    cout << " " << *iter3 << endl;
	cout << endl;
	cout << endl;
	
	list<int> ilist;
	cout << "generate_n(back_inserter(ilist), 5, rand)  " << endl;
	generate_n(back_inserter(ilist), 5, rand);
	for(list<int>::iterator iter4 = ilist.begin(); iter4!=ilist.end(); ++iter4)
	    cout << " " << *iter4;
	cout << endl;
	cout << endl;
	
	
	generate(ilist.begin(), ilist.end(), rand);
	cout << "generate(ilist.begin(), ilist.end(), rand) "<< endl;
	for(list<int>::iterator iter5 = ilist.begin(); iter5!=ilist.end(); ++iter5)
	    cout << " " << *iter5;
	cout << endl;
	cout << endl;
	
	
	return 0;
}
 
