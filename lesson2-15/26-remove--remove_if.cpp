//STL�㷨 -- ɾ���㷨��remove()  remove_if() 
#include<iostream>
#include<list>
#include<algorithm>
#include<vector>

using namespace std;

int main()
{
	list<int> ilist;
	
	for(int i=1;i<=6;++i)
	{
		ilist.push_front(i);
		ilist.push_back(i);
	}
	
	
	cout <<"list<int>::iterator iter1 = ilist.begin() "<<endl;
	for(list<int>::iterator iter1 = ilist.begin(); iter1 != ilist.end(); ++iter1)
	{
		cout <<" "<< *iter1;
	}
	cout << endl;
	cout << endl;
	
	
	list<int>::iterator end;
	end = remove(ilist.begin(), ilist.end(), 3);
	
	
	cout <<"list<int>::iterator iter1 = ilist.begin() "<<endl;
	for(list<int>::iterator iter2 = ilist.begin(); iter2 != end; ++iter2)
	{
		cout <<" "<< *iter2;
	}
	cout << endl;
	cout << endl;
	
	cout << "deleted  " << distance(end, ilist.end()) << " numbers " << endl;
	cout << endl;
	cout << endl;
	
	ilist.erase(end, ilist.end());
	cout <<" ilist.erase(end, ilist.end()) "<<endl;
	for(list<int>::iterator iter3 = ilist.begin(); iter3 != ilist.end(); ++iter3)
	{
		cout <<" "<< *iter3;
	}
	cout << endl;
	cout << endl;
	
	vector<int> ivec;
	for(int i=2; i<=6; ++i)
	    ivec.push_back(i);
	for(int i=4; i<=9; ++i)
	    ivec.push_back(i);
	for(int i=1; i<=7; ++i)
	    ivec.push_back(i);
	    
	cout << "vector<int>::iterator iter4 = ivec.begin() " << endl;
	for(vector<int>::iterator iter4 = ivec.begin();iter4 != ivec.end(); ++iter4)
        cout << " "<< *iter4;
	cout << endl;
	cout << endl;
	
	
	ivec.erase(remove(ivec.begin(),ivec.end(), 5), ivec.end());
	cout <<" ivec.erase(remove(ivec.begin(),ivec.end(), 5), ivec.end()) "<<endl;
	for(vector<int>::iterator iter3 = ivec.begin(); iter3 != ivec.end(); ++iter3)
	{
		cout <<" "<< *iter3;
	}
	cout << endl;
	cout << endl;
	return 0;
} 
