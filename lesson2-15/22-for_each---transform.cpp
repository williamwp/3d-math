//STL算法中,
//for_each()  速度快   不灵活 
//transform()  速度慢   非常灵活 算法比较
#include<iostream>
#include<vector>
#include<list>
#include<algorithm> 

using namespace std;
//必须是引用，按照引用传递，for_each的速度非常快 
void square(int& elem)
{
	elem = elem * elem;
}

//传参数的时候，做一次拷贝 
//返回的时候，又要做一次拷贝 
int square2(int elem)
{
	return elem * elem; 
}


int main()
{
    vector<int> a;
	vector<int> b;
	
	for(int i=1; i<9; ++i)
	{
		a.push_back(i);
		b.push_back(i);
	}
	    
		
	for(vector<int>::iterator iter = a.begin(); iter !=a.end(); ++iter)
	    cout << " vector<int>::iterator iter = a.begin() " << *iter << endl;
	cout << endl;
	cout << endl;
	
	cout << " vector a " << endl;
	for_each(a.begin(), a.end(), square);
	
	for(vector<int>::iterator iter = a.begin(); iter !=a.end(); ++iter)
	    cout << " vector<int>::iterator iter = a.begin() " << *iter << endl;
	cout << endl;
	cout << endl;
	
	
	cout << " vector b " << endl;
	for(vector<int>::iterator iter = b.begin(); iter !=b.end(); ++iter)
	    cout << " vector<int>::iterator iter = b.begin() " << *iter << endl;
	cout << endl;
	cout << endl;
	
	transform(b.begin(),b.end(),
	          b.begin(),
			  square2); 
	
	cout << "after transform， vector b " << endl;
	for(vector<int>::iterator iter = b.begin(); iter !=b.end(); ++iter)
	cout << " vector<int>::iterator iter = b.begin() " << *iter << endl;
	cout << endl;
	cout << endl;
	return 0;
} 
