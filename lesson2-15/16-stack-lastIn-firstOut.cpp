#include<iostream>
#include<stack>
#include<vector>
#include<list>
// stack: LIFO,  s.empty()  s.size()  s.pop()  
// s.top()  s.push(item)
using namespace std;

class A
{
	
	
}; 



int main()
{
	//stack can be constructed by deque, list and vector
	stack<int, deque<int> > a;
	//20	23	C:\Users\william\Desktop\C++\16-stack-lastIn-firstOut.cpp	
	//[Error] '>>' should be '> >' within a nested template argument list
	stack<int, vector<int> > b;
	stack<int, list<int> > c;
	stack<int>  d;
	
	d.push(25);
	d.push(24);
	d.push(23);
	d.push(22);
	d.push(21);
	int d1 = d.top();
	cout << "stack -- last in first out -- d.top()  " << d1 << endl;
	
	d.pop();
	cout << "  after d.pop(),  d.top() is" << d1 << endl;
	
	cout << "the size of stack d is "<<d.size() << endl;
	
	//use loop while to show each value of the stack d
	cout << "d.top(), then d.pop() " << endl;
//	while(d.size() != 0)
    while(d.empty() == false)
	{
		cout << "show each value of stakc d " << d.top() << endl;
		d.pop();
	}
	return 0;
}
