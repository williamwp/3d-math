#include<iostream>

/*
C++使用 try catch 来捕获异常
******************************
try-catch语句的用法：
try
{
    //可能发生异常的代码 
} 

catch(类型 变量)
{
    //对捕捉的异常处理 
}
catch(...)
{
    //对其他异常的处理 
}
******************************
try 区段： 
这个区段中包含了可能发生异常的代码，
在发生了异常之后，需要通过throw抛出。
******************************
catch 子句
每个catch子句都代表着一种异常的处理，catch子句用于处理特定类型的异常。
catch(...)处理任何异常
 ******************************
 throw子句
 throw子句用于抛出异常，被抛出的异常可以是C++的内置类型
 (例如：throw int(1);), 也可以是自定义类型。 
*/

using namespace std;

class A
{
public:


private:
	
	
}; 


int main()
{
	//try catch
/*	
	try
	{
		cout << "before  " << endl;
		int a = 10;
		int b = 0;
		cout <<"a =" << a;
		int c = a/b;
		cout << " after " << c << endl;
		
		
	 } 
	
	catch(...)
	{
		cout << "捕获异常代码 " << endl; 
	}
*/	

    try
    {
    	cout << "before: " << endl;
    	
		//throw "hello";
    	//throw 123;
    	throw 1.2f;
    	
    	
    	cout << " after: " << endl;
	}
	
	catch (const char *p1)
	{
		cout << " catch the exception: " << p1 << " --> ready to solve"<< endl;
	}
		
	catch (int p2)
	{
		cout << " catch the exception: " << p2 << " --> ready to solve"<< endl;
	}
	
	catch (float p3)
	{
		cout << " catch the exception: " << p3 << " --> ready to solve"<< endl;
	}
	
	
	catch(...)
	{
		cout << "捕获到异常，进行处理 " << endl;
	}

	return 0;
}
