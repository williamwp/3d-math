#include<iostream>
#include<cstring>


using namespace std;
/*
“memcpy指的是C和C++使用的内存拷贝函数，
函数原型为void *memcpy(void *destin, void *source, unsigned n)；
函数的功能是从源内存地址的起始位置开始拷贝若干个字节到目标内存地址中，
即从源source中拷贝n个字节到目标destin中。
*/
class A
{
public:
	//如果不加const, 是A(char *str)的话
	//有警告：[Warning] deprecated conversion 
	//from string constant to 'char*' [-Wwrite-strings] 
	A(const char *str) 
	{
		int len = strlen(str)+1;
		p = new char[len];
		memcpy(p, str, len);
	}
	
	void print()
	{
		cout << "called void print(), p is " << p << endl;
	}
	
	A(const A &a1) //拷贝构造函数，深拷贝
    //这样的话，called void print(), p is hello的值继续是hello，
	//不会随着 A b=a; 的改变而改变了 
	{
		int len = strlen(a1.p) + 1;
		p = new char[len];
		memcpy(p, a1.p, len);
		//如果是 memcpy(p, a1, len);的话，会报错 
		//[Error] cannot convert 'const A' to 'const void*' 
		//for argument '2' to 'void* memcpy(void*, const void*, size_t)'
	}
	
	char *p;
	
private:
	
};

class B
{
public:
	B()
	{}
	 
	B(int b1):b(b1)
	{
	    cout << "called B(int b1):b(b1) " << endl; 
	    cout << " b = " << b << "\n\n" << endl;
 	} 

    void printB()
    {
    	cout << " called void printB(), b = " << b << endl;
	}

    int setValue(int b2)
    {
    	b = b2;
    	cout << "called int setValue(int b2), b = " << b << "\n\n" << endl;
	}
	
	int b;
	
}; 

int main()
{
/*	A a("hello");
	a.print();
	
	A b=a; //调用默认的拷贝构造函数，仅仅值拷贝
	//相当于 b.p=a.p 
	b.print();
	
	strcpy(a.p,"world");
	a.print();
	
	b.print();//此刻，b中p的值也被改变了，很明显是错误的
	//因为a.p和b.p指向同一块内存
	//真正情况下，应该给b一块内存。 由此可见，默认的拷贝构造函数是浅拷贝 
*/

    //与定义数组一样：类类型 数组名【N】；
	//数组每个元素都是一个类的实例，即对象 
//	B arr[3];
    cout << "	// 第一种方式 " << endl;
	B arr[3] = {B(11), B(12), B(13)};
	// 第一种方式
	
	
	cout << "	// 第二种方式 " << endl;
	// 第二种方式
	arr[0].b = 100;	
	arr[1].b = 200;
	arr[2].b = 300;
	
	arr[0].printB();
	arr[1].printB();
	arr[2].printB();
	cout << endl;
	cout << endl;
	//B(11).printB();
	//B(12).printB();
	//B(13).printB();
	
	
	cout << "	// 第三种方式 " << endl;
	arr[0].setValue(121);
	arr[1].setValue(122);
	arr[2].setValue(123);
	
	
	cout << "for 循环，遍历类类型的数组 " << endl;
	for(int i=0; i<3;i++)
	{
		arr[i].printB();
    }
	
	
	
	
	return 0;
}
