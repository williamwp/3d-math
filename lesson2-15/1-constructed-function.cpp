#include <iostream>

using namespace std;


class A
{
public:
    A(float b )  // without parameters
    {
        a = 100;
        cout << " called A() constrution function  " << endl;
    }

    A(int a = 111)  // with parameters
    {
        cout << "   called A(int a = 111)  constrution function     " << endl;
        this -> a = a; // this points to the object itself
// if user defines any construction function, the system will not provide constructed function.
    }

private:
    int a;


};


int main()
{
    A a(1.1f);
    A a2(11);

    return 0;

}
