#include<iostream>
#include<queue> 

using namespace std;

//STL 优先级队列 priority_queue; 不能使用list 
//最大值优先级队列，最小值优先级队列
//优先级队列适配器 STL priority_queue
// priority_queue<int, deque<int> >  pq;  priority_queue<int, vector<int> >  pq;
// pq.empty()   pq.size()   pq.top()   pq.pop()   pq.push(item) 


int main()
{
	priority_queue<int>  pq1;
	priority_queue<int, vector<int> >  pq2;  //默认地是使用vector 
	priority_queue<int, deque<int> >   pq3;
	
	pq1.push(1);
	pq1.push(2);
	pq1.push(10);
	pq1.push(3);
	pq1.push(4);
	pq1.push(5);
	
	//最大的值永远在前面
	cout << " the max value always at the top -- " << pq1.top() << endl;
	cout << endl << endl;
	
	cout << " priority_queue pq1.size() is " << pq1.size() << endl; 
	cout << endl << endl;
	
	
	cout << " pq1.pop() deletes the max value every time " << endl;
	while(pq1.empty() == false)
	{
		cout << "pq1.top() is " << pq1.top() << endl;
		pq1.pop();
	}
	cout << endl << endl;
	
	
	//greater是一个谓词，就是一个条件 
	priority_queue<int, deque<int>, greater<int> > pq4;
	pq4.push(21);
	pq4.push(22);
	pq4.push(30);
	pq4.push(23);
	//最小的值永远在前面
	cout << " the min value always at the top -- " << pq1.top() << endl;
	cout << " " << endl;
	
	cout << " pq1.pop() deletes the min value every time " << endl;
	while(pq4.empty() == false)
	{
		cout << "pq4.top() is " << pq4.top() << endl;
		pq4.pop();
	}
	cout << endl << endl;
	
	return 0;
}
