#include<iostream>
#include<list>

using namespace std;

/*
顺序容器 STL list类--双向链表 
实例化std::list对象
在list开头插入元素
在list末尾插入元素
在list中间插入元素
删除list中的元素
对list中元素进行反转和排序 
*/
void PrintList(const list<int>& listInput);

int main()
{
	list<int> a;
	std::list<int>::iterator iter;
	
	//list is not array数组, we can't use array[i] to show the values 
	//when we insert the data in the list, "first in, last show"
	a.push_front(10);
	a.push_front(2001);
	a.push_front(-1);
	a.push_front(9999);
	
	a.push_back(8989);//8989 is behind 10
	
	//we can also insert the data in the middle
	iter = a.begin();
	++iter;
	// through controling the iterator, we can insert many datas
	a.insert(iter, 111);
	a.insert(a.end(), 4, 222); // insert four numbers of 222 
	/*
	 when we insert the data in the list, first in, last show
 std::list<int>::iterator iter; 111
 std::list<int>::iterator iter; 9999
 std::list<int>::iterator iter; -1
 std::list<int>::iterator iter; 2001
 std::list<int>::iterator iter; 10
 std::list<int>::iterator iter; 8989
 std::list<int>::iterator iter; 222
 std::list<int>::iterator iter; 222
 std::list<int>::iterator iter; 222
 std::list<int>::iterator iter; 222
	*/
	
	
	cout <<" when we insert the data in the list, first in, last show " << endl;
	for(iter = a.begin(); iter != a.end(); ++iter)
	{
		
		cout << " std::list<int>::iterator iter; " << *iter << endl;
	 } 
	 
	 cout << endl;
	
	PrintList(a);
	
	return 0;
} 

void PrintList(const list<int>& listInput)
{
	std::list<int>::const_iterator iter;
	cout << "called void PrintList(const list<int>& listInput) " << endl;
	for(iter = listInput.begin(); iter != listInput.end(); ++iter)
	{
		cout << " std::list<int>::iterator iter; " << *iter << endl;
	}
	
}

