//swap_range(b,e,b2)
#include<iostream>
#include<vector>
#include<deque>
#include<algorithm>


using namespace std;

int main()
{
	vector<int> ivec1;
	vector<int> ivec2;
	
	ivec1.push_back(1);
	ivec1.push_back(2);
	ivec1.push_back(3);
	
	ivec2.push_back(10);
	ivec2.push_back(20);
	ivec2.push_back(30);
	
	
	ivec1.swap(ivec2);
	cout << ivec1.at(0) << " "<< ivec1.at(1) <<" "<< ivec1.at(2) ;
	 
	 
// swap()成员函数的速度最快 
    cout << endl;
	cout << endl;
    cout << endl;
	cout << endl;

    vector<int> ivec;
	deque<int>  ideq;
	
	for(int i=1; i<=9; ++i)
	    ivec.push_back(i);
	    
	for(int j = 10; j <= 18; ++j)
	    ideq.push_back(j);
	    
	    
	cout << " vector<int>::iterator iter = ivec.begin() " << endl;
	for(vector<int>::iterator iter = ivec.begin(); iter != ivec.end(); ++iter)
	    cout  << *iter << " ";
	    cout << endl;
	    cout << endl;
	
	cout << " deque<int>::iterator iter1 = ideq.begin() " << endl;
	for(deque<int>::iterator iter1 = ideq.begin(); iter1 != ideq.end(); ++iter1)
	    cout << *iter1 << " ";
	    cout << endl;
	    cout << endl;
	    
	    
	//交换算法swap_ranges
	cout << "begin swap ranges: " << endl;
	deque<int>::iterator position;
	position = swap_ranges(ivec.begin(), ivec.end(), ideq.begin());
	cout << "after swap_ranges, vector<int>::iterator iter = ivec.begin() " << endl;
	for(vector<int>::iterator iter = ivec.begin(); iter != ivec.end(); ++iter)
	    cout << *iter << " ";
	cout << endl;
	cout << endl;
	
	cout << "after swap_ranges,  deque<int>::iterator iter1 = ideq.begin() " << endl;
	for(deque<int>::iterator iter1 = ideq.begin(); iter1 != ideq.end(); iter1++)
	    cout << *iter1 << " ";
	cout << endl;
	cout << endl;
	
	
	if(position == ideq.end())
	{
		cout << " swap_ranges finishes the exchange. " << endl;
	}
	
    
	swap_ranges(ideq.begin(), ideq.begin()+3, 
	             ideq.rbegin());
	cout << "swap_ranges(ideq.begin(), ideq.begin()+3, ideq.rbegin()); " << endl;
	for(deque<int>::iterator iter2 = ideq.begin(); iter2 != ideq.end(); iter2++)
	    cout << *iter2 << " ";
	cout << endl;
	cout << endl;
	return 0;
} 
