#include<iostream>
#include<vector>
#include<list>
#include<algorithm>
#include<ostream>
#include<iterator>

using namespace std;

int main()
{
	vector<int> ivec;
	list<int> ilist;
	
	for(int i = 0; i < 9; i++)
	    ivec.push_back(i);
	    
	for(vector<int>::iterator iter = ivec.begin();iter!=ivec.end(); iter++)
	    cout << " show each value of *iter " << *iter << endl;
	cout << endl;
	cout << endl;
	
	cout << " negate " << endl; 
	transform(ivec.begin(), ivec.end(),
	    ivec.begin(), negate<int>() );
	
	for(vector<int>::iterator iter = ivec.begin();iter!=ivec.end(); iter++)
	    cout << "after transform, show each value of *iter " << *iter << endl;
	cout << endl;
	cout << endl;
	 
	
	transform(ivec.begin(), ivec.end(), back_inserter(ilist),
	    bind2nd(multiplies<int>(), 10) ); 
	
	
	for(list<int>::iterator iter1 = ilist.begin();iter1!=ilist.end(); iter1++)
    cout << "after transform, multiplies<int>(), show each value of *iter1 " << *iter1 << endl;
	cout << endl;
	cout << endl;
	
	cout << "vector ivec ostream_iterator<int>(cout, " "), " << endl;
	transform(ivec.begin(), ivec.end(), ostream_iterator<int>(cout, " "), negate<int>() );
	
	cout << endl;
	cout << endl;
	cout << "list ilist ostream_iterator<int>(cout, " "), " << endl;
	transform(ilist.begin(), ilist.end(), ostream_iterator<int>(cout, " "), negate<int>() );
	
	return 0;
} 
