//STL算法 -- 替换算法
// replace(b,e,ov,nv)  replace_if(b,e,p,v)
// replace_copy(b1,e1,b2,ov,nv)  replace_copy_if(b1,e1,b2,p,v)

#include<iostream>
#include<vector>
#include<list>
#include<algorithm>
#include<iterator>

using namespace std;

int main()
{
	list<int> ilist;
	for(int i=2; i<=7;++i)
	{
		ilist.push_back(i);
	}
	
	for(int i=4; i <= 9; ++i)
	{
		ilist.push_back(i);
	}

    for(list<int>::iterator iter = ilist.begin();
	    iter != ilist.end(); ++iter)
	    cout << " " << *iter;
	cout << endl;
	cout << endl;
	
	replace(ilist.begin(), ilist.end(), 6, 42);
	
	
	cout << "replace_if(ilist.begin(), ilist.end(), bind2nd(less<int>(), 5), 0) " << endl;
	replace_if(ilist.begin(), ilist.end(), bind2nd(less<int>(), 5), 0);
	for(list<int>::iterator iter = ilist.begin();
	    iter != ilist.end(); ++iter)
	    cout << " " << *iter;
	cout << endl;
	cout << endl;
	
	list<int> ilist2;
	for(int i = 1; i < 9; ++i)
	    ilist2.push_back(i);
	cout << "list<int> ilist2 "<< endl;	
	for(list<int>::iterator iter1 = ilist2.begin();
	    iter1 != ilist2.end(); ++iter1)
		cout << " " << *iter1;
	cout << endl;
	cout << endl;	
	
	cout << "list<int> ilist2, replace_copy "<< endl;
	replace_copy(ilist2.begin(), ilist2.end(), ostream_iterator<int>(cout, " "), 5, 115);  
	cout << endl;

	
	//预定义的函数适配器 bind2nd 
	//预定义的函数对象less 
	cout <<"replace_copy_if, bind2nd(less<int>(), 5), 42 " << endl;
	replace_copy_if(ilist2.begin(), ilist2.end(), ostream_iterator<int>(cout, " "),
	                bind2nd(less<int>(), 6), 42);
	cout << endl;
	cout << endl;
	
	
	replace_copy_if(ilist2.begin(), ilist2.end(), ostream_iterator<int>(cout, " "), 
	               bind2nd(modulus<int>(), 2), 0);
	
	for(list<int>::iterator iter2 = ilist2.begin();
	    iter2 != ilist2.end(); ++iter2)
		cout << " " << *iter2;    
    cout << endl;
	cout << endl;
    
	return 0;
}
 
