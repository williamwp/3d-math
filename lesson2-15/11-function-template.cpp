/*  函数模板 
template <class或typename T>
返回类型 函数名（形参表）
{
    函数定义体 
}  

模板函数表示的是由一个函数模板生成而来的函数。 

template <class T>
T sum(T a, T b)
{
    return a+b;
}

----sum<int>(12, 23)
int sum(int a, int b)
{
    return a+b;
}


----sum<ifloat>(1.2f, 2.3f)
float sum(float a, float b)
{
    return a+b;
}

----sum<double>(1.2, 2.3)
double sum(double a, double b)
{
    return a+b; 
}


****************
3 模板类型参数名可以被用来制定函数模板的返回位
template<class T1, class T2, class T3>
T3 sum(T1 a, T2 b)
{
    // 三个的类型可不同 
    T3 tmp =(a+b);
    return tmp;
} 


****************
4 模板参数名在同一模板参数表中只能被使用一次，但是模板
参数名可以在多个函数模板声明或定义之间被重复使用

// 错误：模板参数名T的非法重复定义
template<class T, class T>
T sum(T a, T b)
{
    T tmp = (a+b);
    return tmp;
} 


****************
5 如果一个函数模板有一个以上的模板类型参数,
则每个模板类型参数前面都必须有关键字class 或typename
//错误：模板参数名 T2的非法
template<class T, T2>
T sum(T a, T2 b)
{
    T tmp=(a+b);
    return tmp;
} 
*/

#include<iostream>

using namespace std;


/*
**************************** 类模板
template<class或typename T>
class 类名
{
    。。。。 
}; 
**************************** 
template<class T>
class Math
{
public:
    //成员函数在内部实现
	T sum(T a, T b)
	{
	    return a+b;
	} 
}; 
**************************** 
在类定义体外定义成员函数时，还需在类体外进行模板声明
template<class T>
返回值 类名<T>::成员函数（）
{
    。。。 
} 
**************************** 
template<class T>
class Math
{
public:
    T sum(T a, T b);
    
}; 
//成员函数在外部实现
template<class T>
T Math<T>::sum(T a, T b)
{
    return a+b;
} 

****************************
模板类表示的是由一个类模板生成而来的类。 

*/
template<typename T>
class Math 
{
public:
    T sum(T a, T b);//在类内部声明，类外部实现
	T max(T a, T b)
	{
		return a > b ? a : b;
	}
	// 类内部声明，类内部实现


private:	
	
}; 

template<class T>
T Math<T>::sum(T a, T b)
{
	return a+b;
}



template <class T>
T sum(T a, T b)
{
	return a+b;
}


int main()
{
	
	cout <<" sum<int>(111, 222) is " <<sum<int>(111, 222) << endl;
	cout << " sum<float>(111.1f, 222.2f) is "<< sum<float>(111.1f, 222.2f) << endl;
	cout << " sum<double>(111.1, 222.2) is " << sum<double>(111.1, 222.2) << endl;
	
	
	//类模板  类型 对象 
	Math<int> m; 
	cout << " m.sum(1, 2)  is  "  << m.sum(1, 2) << endl;
	cout << " m.max(1,2) " << m.max(1,2) << endl;
	//模板类函数类的声明与实现必须放在同一个.h文件内 
	return 0;
}
