# command  

# wangpeng@rongtian-SYS-7049GP-TRT:~/backup6/hybrid-rendering/fractalrender-main$ ./fractalrender a
where a is a directory

# wangpeng@rongtian-SYS-7049GP-TRT:~/backup6/hybrid-rendering/fractalrender-main$ ffmpeg -y -i a/frame_00020.png -framerate 5 out.mp4
ffmpeg version 3.4.11-0ubuntu0.1 Copyright (c) 2000-2022 the FFmpeg developers
  built with gcc 7 (Ubuntu 7.5.0-3ubuntu1~18.04)
  configuration: --prefix=/usr --extra-version=0ubuntu0.1 --toolchain=hardened --libdir=/usr/lib/x86_64-linux-gnu --incdir=/usr/include/x86_64-linux-gnu --enable-gpl --disable-stripping --enable-avresample --enable-avisynth --enable-gnutls --enable-ladspa --enable-libass --enable-libbluray --enable-libbs2b --enable-libcaca --enable-libcdio --enable-libflite --enable-libfontconfig --enable-libfreetype --enable-libfribidi --enable-libgme --enable-libgsm --enable-libmp3lame --enable-libmysofa --enable-libopenjpeg --enable-libopenmpt --enable-libopus --enable-libpulse --enable-librubberband --enable-librsvg --enable-libshine --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libssh --enable-libtheora --enable-libtwolame --enable-libvorbis --enable-libvpx --enable-libwavpack --enable-libwebp --enable-libx265 --enable-libxml2 --enable-libxvid --enable-libzmq --enable-libzvbi --enable-omx --enable-openal --enable-opengl --enable-sdl2 --enable-libdc1394 --enable-libdrm --enable-libiec61883 --enable-chromaprint --enable-frei0r --enable-libopencv --enable-libx264 --enable-shared
  libavutil      55. 78.100 / 55. 78.100
  libavcodec     57.107.100 / 57.107.100
  libavformat    57. 83.100 / 57. 83.100
  libavdevice    57. 10.100 / 57. 10.100
  libavfilter     6.107.100 /  6.107.100
  libavresample   3.  7.  0 /  3.  7.  0
  libswscale      4.  8.100 /  4.  8.100
  libswresample   2.  9.100 /  2.  9.100
  libpostproc    54.  7.100 / 54.  7.100
[png_pipe @ 0x55cde4dae900] Stream #0: not enough frames to estimate rate; consider increasing probesize
Input #0, png_pipe, from 'a/frame_00020.png':
  Duration: N/A, bitrate: N/A
    Stream #0:0: Video: png, rgba(pc), 4800x4800, 25 tbr, 25 tbn, 25 tbc
Stream mapping:
  Stream #0:0 -> #0:0 (png (native) -> h264 (libx264))
Press [q] to stop, [?] for help
[libx264 @ 0x55cde4db4960] using cpu capabilities: MMX2 SSE2Fast SSSE3 SSE4.2 AVX FMA3 BMI2 AVX2 AVX512
[libx264 @ 0x55cde4db4960] profile High 4:4:4 Predictive, level 6.0, 4:4:4 8-bit
[libx264 @ 0x55cde4db4960] 264 - core 152 r2854 e9a5903 - H.264/MPEG-4 AVC codec - Copyleft 2003-2017 - http://www.videolan.org/x264.html - options: cabac=1 ref=3 deblock=1:0:0 analyse=0x1:0x111 me=hex subme=7 psy=1 psy_rd=1.00:0.00 mixed_ref=1 me_range=16 chroma_me=1 trellis=1 8x8dct=0 cqm=0 deadzone=21,11 fast_pskip=1 chroma_qp_offset=4 threads=128 lookahead_threads=16 sliced_threads=0 nr=0 decimate=1 interlaced=0 bluray_compat=0 constrained_intra=0 bframes=3 b_pyramid=2 b_adapt=1 b_bias=0 direct=1 weightb=1 open_gop=0 weightp=2 keyint=250 keyint_min=25 scenecut=40 intra_refresh=0 rc_lookahead=40 rc=crf mbtree=1 crf=23.0 qcomp=0.60 qpmin=0 qpmax=69 qpstep=4 ip_ratio=1.40 aq=1:1.00
Output #0, mp4, to 'out.mp4':
  Metadata:
    encoder         : Lavf57.83.100
    Stream #0:0: Video: h264 (libx264) (avc1 / 0x31637661), yuv444p, 4800x4800, q=-1--1, 25 fps, 12800 tbn, 25 tbc
    Metadata:
      encoder         : Lavc57.107.100 libx264
    Side data:
      cpb: bitrate max/min/avg: 0/0/0 buffer size: 0 vbv_delay: -1
frame=    1 fps=0.4 q=28.0 Lsize=    5203kB time=00:00:00.00 bitrate=546444820.5kbits/s speed=3e-05x    
video:5202kB audio:0kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 0.015018%
[libx264 @ 0x55cde4db4960] frame I:1     Avg QP:26.46  size:5326346
[libx264 @ 0x55cde4db4960] mb I  I16..4: 80.3%  0.0% 19.7%
[libx264 @ 0x55cde4db4960] coded y,u,v intra: 35.5% 36.1% 36.1%
[libx264 @ 0x55cde4db4960] i16 v,h,dc,p: 31% 19% 28% 22%
[libx264 @ 0x55cde4db4960] i4 v,h,dc,ddl,ddr,vr,hd,vl,hu: 14% 14% 30%  8%  7%  7%  7%  6%  6%
[libx264 @ 0x55cde4db4960] kb/s:1065269.25



# fractalrender

fractalrender is a utility to generate visuals of fractals of the [Mandelbrot fractal](https://en.wikipedia.org/wiki/Mandelbrot_set). 

## Install

TODO: make installable

## Build

Building fractalrender requires a C++ compiler and `make`. If you have those dependencies, building is as simple as:

```shell
$ make
```

## Usage

You can run `fractalrender` (or `./fractalrender` if built locally) with `--help` to print usage information:

```shell
$ ./fractalrender --help
```


## Common Keys

The keys (`name=val` pairs) are up to each engine (some engines may or may not support any of these):

  * `iter=100`: Gives the number of iterations to render the fractal
  * `color=01295F,BDD5EA`: Gives the color scheme, as a list of colors. Each color is expected to be in HEX RGB (Red, Green, Blue components) and colors are seperated by commas
  * `colorscale=0.25`: Multiplies the color scale before sampling. Higher values mean more rings of color, lower values mean more gradual changes
  * `coloroffset=0.0`: Adds a constant offset to the color scale before sampling. Basically shifts the color scheme
  * `bsr=0`: Binary splitting on real component, which controls the tree-like effect. Use `=0` for off, `=1` for on
  * `bsi=0`: Binary splitting on imaginary component, which controls the tree-like effect. Use `=0` for off, `=1` for on
