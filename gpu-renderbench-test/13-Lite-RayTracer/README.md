# 1 Introduction
This code implements a path tracing renderer. Path tracing is a realistic ray tracing algorithm used to generate lifelike images. The code utilizes the CUDA programming model to leverage the parallel computing power of GPUs for accelerated rendering.

The code defines various constants and variables, including window size, sample count per pixel, polygon count, etc. It uses CUDA functions to allocate memory on both the host and GPU device for storing the rendered frame buffer and other required data.

Next, the code defines functions for adding polygons and cubes to construct the scene. It also includes auxiliary functions for calculating intersections, generating random directions, and so on.

The core of the code is the render_kernel function, which performs the path tracing algorithm on the GPU. This function generates ray directions based on the position of each pixel and random numbers. It then iteratively computes intersections, reflections, refractions, etc., between the rays and objects in the scene, accumulating the color of the rays. Finally, the accumulated color values are stored in the output buffer.

Additionally, there is a path_tracing function that controls the number of iterations for the path tracing algorithm and stores the final rendered result in the specified buffer.

Overall, this code implements a path tracing renderer accelerated by CUDA, capable of generating realistic images. By harnessing the parallel computing power of GPUs, the rendering process is accelerated.

# 2 Command 
$ ./a.out  


