# 1 Introduction
The program implements a partial photon mapping algorithm using CUDA, which is a global illumination technique used in computer graphics for realistic light interactions. It includes various data structures and functions related to the algorithm, such as Vec3 for 3D vector operations, BoundingBox for bounding box representation, HitPoint for storing surface information, Photon for representing emitted photons, and a linked list data structure for efficient storage. The code also defines global variables, a hash function for spatially hashing hit points, and functions for building the hash grid, performing ray-sphere intersection, generating photons, and tracing rays from the camera. However, the code appears incomplete, and additional code might be necessary to complete the algorithm and utilize CUDA-specific functions for parallelization on a GPU.

# 2 Command 
$ ./a.out  


