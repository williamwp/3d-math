# 1 Introduction

A bare-bones but polished fluid simulator and volumetric renderer written in CUDA C/C++ in ~600 LOC.

The organization is based on Philip Rideout's 2D OpenGL simulator (http://prideout.net/blog/?p=58),  
as well as George Corney's interactive WebGL demo (https://github.com/haxiomic/GPU-Fluid-Experiments)

This program is a CUDA C/C++ implementation consisting of approximately 600 lines of code. It serves as a minimal yet refined solution for simulating fluids and rendering volumetric data. The software leverages the power of CUDA, a parallel computing platform, to achieve efficient and high-performance execution on compatible GPUs.

Key characteristics and capabilities of this fluid simulator and volumetric renderer include:

Fluid Simulation: The software employs computational fluid dynamics techniques to simulate the behavior and dynamics of fluids. It can model and simulate various fluid properties such as velocity, density, and pressure. The simulation algorithm calculates the fluid's motion and interactions based on physical principles, resulting in realistic fluid behavior and motion.

Volumetric Rendering: The renderer can visualize and render volumetric data, such as three-dimensional scalar fields or voxel grids. It utilizes ray marching or similar techniques to generate images or animations from the volumetric data. This enables the creation of visually appealing and detailed representations of the simulated fluid or other volumetric effects.

CUDA Optimization: The implementation is specifically designed to leverage the parallel processing capabilities of CUDA-enabled GPUs. By distributing computational tasks across multiple threads and utilizing GPU memory efficiently, the simulator and renderer can achieve high-performance execution and faster simulation and rendering times.

Minimalistic and Polished: Despite its concise codebase, the software aims for a polished and refined user experience. It focuses on the essential components necessary for fluid simulation and volumetric rendering while ensuring the implementation is well-structured, readable, and efficient. The simplicity of the codebase makes it a valuable learning resource and a starting point for further customization and expansion.  


# 2 Command 

In ```build.sh```, check that the path to your cuda installation is correct. Then run ```build.sh``` to create an executable.  
$ ./run_fliud  


![](https://i.imgur.com/qKtCdZf.png "Render")

Navier Tokes yo

![](https://i.imgur.com/uYr2u7y.png "Render")

![](https://i.imgur.com/Y3MGgck.png "Render")

![](https://i.imgur.com/g8OmfZA.png "Render")

![](https://i.imgur.com/dMWps1a.png "Render")

Testing the renderer with spheres

![](https://i.imgur.com/un8Smjb.jpg "Render")
