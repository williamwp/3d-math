# GPU-RenderBench
## 1 Introduction
GPU-RenderBench is a tool designed to test and evaluate the performance of graphics processing units (GPUs). It aims to assist developers, researchers, and hardware manufacturers in measuring and comparing the performance of different GPUs in rendering tasks.

Here are some features and functionalities of GPU-RenderBench:

Render benchmark tests: GPU-RenderBench provides a series of benchmark test scenarios that simulate various graphics rendering tasks, such as ray tracing, volume rendering, and photon mapping. These benchmark tests measure the performance and efficiency of GPUs in different scenarios.

Configurable parameters: Users can adjust and customize the benchmark tests according to their needs and hardware configurations. This allows for better adaptation to different GPU architectures and performance levels, resulting in accurate performance data.

Multi-platform support: GPU-RenderBench can run on multiple operating systems, including Windows, Linux, and macOS. This enables users to perform GPU performance comparisons and evaluations on different platforms.

Visual interface: GPU-RenderBench provides an intuitive visual interface that allows users to easily configure test parameters, run benchmark tests, and view results. This simplifies the testing process and provides intuitive performance comparison and analysis tools.

Data export and analysis: GPU-RenderBench supports exporting test data in CSV or JSON format, facilitating further data analysis and visualization.

By using GPU-RenderBench, users can accurately evaluate the performance of different GPUs and compare their capabilities in graphics rendering. This is valuable for professionals in areas such as game development, graphic design, scientific computing, and hardware evaluation. It helps in selecting the right GPU for specific requirements and optimizing the performance of related applications.


## 2 Dependencies

Following dependencies were used in this project:

* CUDA SDK 10.1
* OpenGL 4.6.0
* GNU Make 4.2.1

## 3 Build and Run
### Linux Makefile Build Instructions:
Change the current directory to the sample directory you wish to build, and run make:
```
$ cd dir  
$ make
```    

### Linux CMakeLists.txt Build Instructions:

You will need NVIDIA CUDA for this to compile properly.

```
mkdir build
cd build
cmake ..
make
```





