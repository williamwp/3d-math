# 1 Introduction  
This is a simple voxelization engine made for MIT's 6.807 Computational Fabrication. Given a .obj mesh, this will output a voxel grid in the form of another .obj file. This voxelizer relies on CUDA for acceleration of the inherently parallel voxelization process.

**WARNING**: this can generate some BIG files if you specify a .obj output and make your computer very sad. Keep the resolution below around 128 for safety. Higher can be achieved, but you will want to use binvox output.

Using resolution of 256 I generated a 4.7 GB voxel obj file containing over 67 million vertices. This just about crashed my computer as I obliterated my poor swap space. On the bright side, it only took <2 seconds to generate...

# 2 Command 

./voxelizer [options] [input path] [ouput path]
e.g.  
$ ./voxelizer -r 640 ../../data/sphere/sphere.obj   ../../data/sphere/sphere_voxelized


Options: 

    -s, --samples     : number of sample rays per vertex    

    -v, --verbose     : Verbosity level. Multiple flags for more verbosity.    

    -r, --resolution  : voxelization resolution (default 32)

    -f, --format      : output format - obj|binvox (default binvox)

    -d, --double      : treat mesh as double-thick

    -h, --help        : Displays usage information and exits.

Arguments:

    [input path] : path to .obj mesh

    [ouput path] : path to save voxel grid

Example usage: 

64x64x64 resolution, output to ./data/sphere/sphere_voxelized.binvox
```
./voxelizer -r 64 ./data/sphere/sphere.obj ./data/sphere/sphere_voxelized
```

64x64x64 with 11 randomized direction samples to work with a broken mesh
```
./voxelizer -r 64 -s 11 ./data/sphere/broken_sphere.obj ./data/sphere/broken_sphere_voxelized
```

### Build instructions:

You will need NVIDIA CUDA for this to compile properly.

```
mkdir build
cd build
cmake ..
make
```

