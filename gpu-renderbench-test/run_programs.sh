#!/bin/bash

echo '==========================='
echo '  GPU Rendering Benchmark  '
echo '==========================='
echo ''

sleep 3

echo '3-progressive-photon-mapping'
cd 3-progressive-photon-mapping/
./a.out  
cd ..
echo ''

echo '4-Film-grain-rendering'
cd 4-Film-grain-rendering/bin
./film_grain_rendering_main 2.png 22.png
cd ../..
echo ''

echo '7-N-body-renering'
cd 7-N-body-renering/build
./nbody
cd ../..
echo ''
: '
echo '8-Mandelbrot-rendering'
cd 8-Mandelbrot-rendering
./mandelbrot
cd ..
echo ''

echo '10-Cuda-ray-shadows'
cd 10-Cuda-ray-shadows
./a.out 
cd ..
echo ''
'
echo '13-Lite-RayTracer'
cd 13-Lite-RayTracer
./a.out 
cd ..
echo ''

echo '30-Newtonian-rasterization'
cd 30-Newtonian-rasterization
./bhfs
cd ..
echo ''

echo '47-Ray-tracing-SDL'
cd 47-Ray-tracing-SDL
./a.out 
cd ..
echo ''

echo '70-cuda-rasterizer'
cd 70-cuda-rasterizer
./rasterizer --cuda
cd ..
echo ''

echo '75-octree-raytracer-engine'
cd 75-octree-raytracer-engine
./run -s scene3.json
cd ..
echo ''

echo '91-tsdf-fusion'
cd 91-tsdf-fusion
./a.out 
cd ..
echo ''

echo '92-voxelizer'
cd 92-voxelizer/build/bin
./voxelizer -r 640 ../../data/sphere/sphere.obj   ../../data/sphere/sphere_voxelized
cd ../../..
echo ''

echo '93-cuda-raycasting'
cd 93-cuda-raycasting
./cudarc
cd ..
echo ''

echo '94-animation'
cd 94-animation
./main gpu 100
cd ..
echo ''

echo '95-buddhabrot-fractal'
cd 95-buddhabrot-fractal
./cudabrot -w 200 -h 200 -m 10000 -c 8000 -t 30 -g 1.0
cd ..
echo ''

echo '97-fluid'
cd 97-fluid
./run_fluid
cd ..
echo ''

echo '==============================================='
echo '  All of the rendering programs have finished  '
echo '==============================================='

