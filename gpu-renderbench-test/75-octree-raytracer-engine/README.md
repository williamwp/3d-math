# 1 Introduction 
The code implements various functions related to ray tracing and octree intersection. It includes header files such as <parsing.h> and <display.h>, as well as standard C++ headers like <cstdio> and <iostream>.

The code defines several data structures such as vectors (allObjects, allMeshes, allLights), maps (allMaterials, t_map), and various helper functions for vector operations (iniVec, copyVec, printVec, etc.). It also includes kernel functions prefixed with __device__ that perform operations like finding the minimum/maximum of two values, vector normalization, vector arithmetic, and ray-triangle intersection.

Additionally, there are functions for initializing and manipulating stacks (initStack, copyRay, stackPush, etc.) and octree-related operations (printNode2, initOctStack, octStackPush, etc.). The code contains functions for intersecting spheres (intersectSphere) and triangles (intersectTriangle) with rays, as well as intersecting children nodes of an octree (intersectChildren).

## 2 Compile
At the root:
`make`

## 3 Run
`$ ./a.out -s scene3.json `

