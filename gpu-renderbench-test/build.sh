#!/bin/bash

echo '================================='
echo '  build GPU Rendering Benchmark  '
echo '================================='

echo '3-progressive-photon-mapping'
cd 3-progressive-photon-mapping
make
cd ..
echo ''

echo '4-Film-grain-rendering'
cd 4-Film-grain-rendering
make
cp 2.png bin/
cp 22.png bin/
cd ..
echo ''

echo '7-N-body-renering'
cd 7-N-body-renering
mkdir build
cd build
cmake ..
make
cd ../..
echo ''

echo '8-Mandelbrot-rendering'
cd 8-Mandelbrot-rendering
make
cd ..
echo ''

echo '10-Cuda-ray-shadows'
cd 10-Cuda-ray-shadows
make
cd ..
echo ''

echo '13-Lite-RayTracer'
cd 13-Lite-RayTracer
make 
cd ..
echo ''

echo '30-Newtonian-rasterization'
cd 30-Newtonian-rasterization
make
cd ..
echo ''

echo '47-Ray-tracing-SDL'
cd 47-Ray-tracing-SDL
make
cd ..
echo ''

echo '70-cuda-rasterizer'
cd 70-cuda-rasterizer
make 
cd ..
echo ''

echo '75-octree-raytracer-engine'
cd 75-octree-raytracer-engine
make
cd ..
echo ''

echo '91-tsdf-fusion'
cd 91-tsdf-fusion
nvcc main.cu `pkg-config --libs opencv`
cd ..
echo ''

echo '92-voxelizer'
cd 92-voxelizer
mkdir build
cd build
cmake ..
make
cd bin
./voxelizer -r 640 ../../data/sphere/sphere.obj   ../../data/sphere/sphere_voxelized
cd ../..
echo ''

echo '93-cuda-raycasting'
cd 93-cuda-raycasting
make
cd ..
echo ''

echo '94-animation'
cd 94-animation
make
cd ..
echo ''

echo '95-buddhabrot-fractal'
cd 95-buddhabrot-fractal
make
cd ..
echo ''

echo '97-fluid'
cd 97-fluid
make
cd ..
echo ''

echo '==========================================='
echo '  All of the programs compiled completely  '
echo '==========================================='
