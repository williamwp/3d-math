gpus_init [33m1.109760[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.094480[0m  px_shdr [32m33.219[0m  px/s [94m15,605[0m  prim/s [31m1,734[0m  rays/s [32m124,844[0m  ints/s [94m7,191,008,009[0m  ppm [32m0.018[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.061499[0m  px_shdr [32m6.662[0m  px/s [94m77,816[0m  prim/s [31m8,646[0m  rays/s [32m622,528[0m  ints/s [94m35,857,623,901[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.053116[0m  px_shdr [32m6.715[0m  px/s [94m77,204[0m  prim/s [31m8,578[0m  rays/s [32m617,635[0m  ints/s [94m35,575,792,123[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.052914[0m  px_shdr [32m6.698[0m  px/s [94m77,392[0m  prim/s [31m8,599[0m  rays/s [32m619,136[0m  ints/s [94m35,662,231,637[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.050206[0m  px_shdr [32m6.636[0m  px/s [94m78,117[0m  prim/s [31m8,680[0m  rays/s [32m624,932[0m  ints/s [94m35,996,093,452[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.098937[0m  px_shdr [32m6.662[0m  px/s [94m77,813[0m  prim/s [31m8,646[0m  rays/s [32m622,500[0m  ints/s [94m35,856,008,954[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.051841[0m  px_shdr [32m6.640[0m  px/s [94m78,071[0m  prim/s [31m8,675[0m  rays/s [32m624,565[0m  ints/s [94m35,974,938,005[0m  ppm [32m0.004[0m  

gpus_free [33m4.327696[0m
gpus_init [33m1.288304[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.083637[0m  px_shdr [32m6.659[0m  px/s [94m77,853[0m  prim/s [31m8,650[0m  rays/s [32m622,820[0m  ints/s [94m35,874,438,648[0m  ppm [32m0.004[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.049187[0m  px_shdr [32m6.635[0m  px/s [94m78,133[0m  prim/s [31m8,681[0m  rays/s [32m625,068[0m  ints/s [94m36,003,913,826[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.050497[0m  px_shdr [32m6.681[0m  px/s [94m77,595[0m  prim/s [31m8,622[0m  rays/s [32m620,759[0m  ints/s [94m35,755,712,241[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.048483[0m  px_shdr [32m6.610[0m  px/s [94m78,421[0m  prim/s [31m8,713[0m  rays/s [32m627,370[0m  ints/s [94m36,136,498,435[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.051343[0m  px_shdr [32m6.735[0m  px/s [94m76,966[0m  prim/s [31m8,552[0m  rays/s [32m615,727[0m  ints/s [94m35,465,847,901[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.049052[0m  px_shdr [32m6.689[0m  px/s [94m77,496[0m  prim/s [31m8,611[0m  rays/s [32m619,967[0m  ints/s [94m35,710,090,072[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.047543[0m  px_shdr [32m6.688[0m  px/s [94m77,511[0m  prim/s [31m8,612[0m  rays/s [32m620,085[0m  ints/s [94m35,716,901,442[0m  ppm [32m0.004[0m  

gpus_free [33m0.151266[0m
gpus_init [33m0.788274[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.219038[0m  px_shdr [32m13.356[0m  px/s [94m38,815[0m  prim/s [31m4,313[0m  rays/s [32m310,523[0m  ints/s [94m17,886,131,151[0m  ppm [32m0.004[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.114369[0m  px_shdr [32m13.427[0m  px/s [94m38,610[0m  prim/s [31m4,290[0m  rays/s [32m308,878[0m  ints/s [94m17,791,392,359[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.074080[0m  px_shdr [32m13.231[0m  px/s [94m39,180[0m  prim/s [31m4,353[0m  rays/s [32m313,443[0m  ints/s [94m18,054,336,234[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.075355[0m  px_shdr [32m13.241[0m  px/s [94m39,152[0m  prim/s [31m4,350[0m  rays/s [32m313,215[0m  ints/s [94m18,041,201,390[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.077072[0m  px_shdr [32m13.277[0m  px/s [94m39,045[0m  prim/s [31m4,338[0m  rays/s [32m312,357[0m  ints/s [94m17,991,787,200[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.068939[0m  px_shdr [32m13.210[0m  px/s [94m39,243[0m  prim/s [31m4,360[0m  rays/s [32m313,941[0m  ints/s [94m18,083,013,250[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.064521[0m  px_shdr [32m13.211[0m  px/s [94m39,240[0m  prim/s [31m4,360[0m  rays/s [32m313,919[0m  ints/s [94m18,081,755,296[0m  ppm [32m0.004[0m  

gpus_free [33m0.151348[0m
gpus_init [33m0.793371[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.173374[0m  px_shdr [32m39.780[0m  px/s [94m13,032[0m  prim/s [31m1,448[0m  rays/s [32m104,252[0m  ints/s [94m6,004,926,723[0m  ppm [32m0.004[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.072407[0m  px_shdr [32m13.235[0m  px/s [94m39,170[0m  prim/s [31m4,352[0m  rays/s [32m313,362[0m  ints/s [94m18,049,624,193[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.073921[0m  px_shdr [32m13.220[0m  px/s [94m39,213[0m  prim/s [31m4,357[0m  rays/s [32m313,703[0m  ints/s [94m18,069,311,441[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.074208[0m  px_shdr [32m13.191[0m  px/s [94m39,298[0m  prim/s [31m4,366[0m  rays/s [32m314,385[0m  ints/s [94m18,108,587,492[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.075389[0m  px_shdr [32m13.185[0m  px/s [94m39,316[0m  prim/s [31m4,368[0m  rays/s [32m314,528[0m  ints/s [94m18,116,836,786[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.070168[0m  px_shdr [32m13.235[0m  px/s [94m39,168[0m  prim/s [31m4,352[0m  rays/s [32m313,345[0m  ints/s [94m18,048,657,503[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.065097[0m  px_shdr [32m13.206[0m  px/s [94m39,254[0m  prim/s [31m4,362[0m  rays/s [32m314,031[0m  ints/s [94m18,088,202,330[0m  ppm [32m0.004[0m  

gpus_free [33m0.151938[0m
gpus_init [33m0.794483[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.362903[0m  px_shdr [32m614.315[0m  px/s [94m844[0m  prim/s [31m94[0m  rays/s [32m6,751[0m  ints/s [94m388,853,482[0m  ppm [32m0.004[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.182056[0m  px_shdr [32m983.638[0m  px/s [94m527[0m  prim/s [31m59[0m  rays/s [32m4,216[0m  ints/s [94m242,852,320[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.193618[0m  gpus_init [33m0.793237[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.116727[0m  px_shdr [32m33.212[0m  px/s [94m15,609[0m  prim/s [31m1,734[0m  rays/s [32m124,870[0m  ints/s [94m7,192,540,611[0m  ppm [32m0.009[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.080083[0m  px_shdr [32m6.643[0m  px/s [94m78,043[0m  prim/s [31m8,671[0m  rays/s [32m624,343[0m  ints/s [94m35,962,129,734[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.055169[0m  px_shdr [32m6.602[0m  px/s [94m78,516[0m  prim/s [31m8,724[0m  rays/s [32m628,129[0m  ints/s [94m36,180,235,689[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.054700[0m  px_shdr [32m6.630[0m  px/s [94m78,189[0m  prim/s [31m8,688[0m  rays/s [32m625,508[0m  ints/s [94m36,029,282,719[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.054567[0m  px_shdr [32m6.649[0m  px/s [94m77,970[0m  prim/s [31m8,663[0m  rays/s [32m623,758[0m  ints/s [94m35,928,466,549[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.052988[0m  px_shdr [32m36.355[0m  px/s [94m14,259[0m  prim/s [31m1,584[0m  rays/s [32m114,074[0m  ints/s [94m6,570,678,177[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.058249[0m  px_shdr [32m6.678[0m  px/s [94m77,629[0m  prim/s [31m8,625[0m  rays/s [32m621,028[0m  ints/s [94m35,771,213,844[0m  ppm [32m0.004[0m  

gpus_free [33m0.180593[0m
gpus_init [33m0.835042[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.213135[0m  px_shdr [32m920.232[0m  px/s [94m563[0m  prim/s [31m63[0m  rays/s [32m4,507[0m  ints/s [94m259,585,362[0m  ppm [32m0.004[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.273251[0m  px_shdr [32m1356.513[0m  px/s [94m382[0m  prim/s [31m42[0m  rays/s [32m3,057[0m  ints/s [94m176,097,650[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.293696[0m  px_shdr [32m1152.826[0m  px/s [94m450[0m  prim/s [31m50[0m  rays/s [32m3,597[0m  ints/s [94m207,211,437[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.228379[0m  px_shdr [32m1246.507[0m  px/s [94m416[0m  prim/s [31m46[0m  rays/s [32m3,327[0m  ints/s [94m191,638,441[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.223110[0m  px_shdr [32m1226.180[0m  px/s [94m423[0m  prim/s [31m47[0m  rays/s [32m3,382[0m  ints/s [94m194,815,318[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.212857[0m  px_shdr [32m1222.374[0m  px/s [94m424[0m  prim/s [31m47[0m  rays/s [32m3,393[0m  ints/s [94m195,421,966[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.156484[0m  px_shdr [32m991.805[0m  px/s [94m523[0m  prim/s [31m58[0m  rays/s [32m4,181[0m  ints/s [94m240,852,571[0m  ppm [32m0.004[0m  

gpus_free [33m0.200413[0m
gpus_init [33m1.005727[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.893650[0m  px_shdr [32m82.393[0m  px/s [94m6,292[0m  prim/s [31m699[0m  rays/s [32m50,334[0m  ints/s [94m2,899,250,162[0m  ppm [32m0.006[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.955434[0m  px_shdr [32m78.041[0m  px/s [94m6,643[0m  prim/s [31m738[0m  rays/s [32m53,141[0m  ints/s [94m3,060,924,953[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m1.047339[0m  px_shdr [32m36.937[0m  px/s [94m14,035[0m  prim/s [31m1,559[0m  rays/s [32m112,279[0m  ints/s [94m6,467,267,292[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.915967[0m  px_shdr [32m73.063[0m  px/s [94m7,095[0m  prim/s [31m788[0m  rays/s [32m56,762[0m  ints/s [94m3,269,485,573[0m  ppm [32m0.006[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.995993[0m  px_shdr [32m78.393[0m  px/s [94m6,613[0m  prim/s [31m735[0m  rays/s [32m52,903[0m  ints/s [94m3,047,199,548[0m  ppm [32m0.005[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.992977[0m  px_shdr [32m39.633[0m  px/s [94m13,080[0m  prim/s [31m1,453[0m  rays/s [32m104,640[0m  ints/s [94m6,027,271,467[0m  ppm [32m0.006[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.931785[0m  px_shdr [32m82.796[0m  px/s [94m6,261[0m  prim/s [31m696[0m  rays/s [32m50,089[0m  ints/s [94m2,885,137,022[0m  ppm [32m0.004[0m  

gpus_free [33m0.214467[0m
gpus_init [33m3.099202[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.185610[0m  px_shdr [32m1594.441[0m  px/s [94m325[0m  prim/s [31m36[0m  rays/s [32m2,601[0m  ints/s [94m149,819,702[0m  ppm [32m0.004[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.167948[0m  px_shdr [32m1183.566[0m  px/s [94m438[0m  prim/s [31m49[0m  rays/s [32m3,504[0m  ints/s [94m201,829,647[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.168540[0m  px_shdr [32m1208.568[0m  px/s [94m429[0m  prim/s [31m48[0m  rays/s [32m3,431[0m  ints/s [94m197,654,274[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.168827[0m  px_shdr [32m1206.787[0m  px/s [94m430[0m  prim/s [31m48[0m  rays/s [32m3,437[0m  ints/s [94m197,946,004[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.168698[0m  px_shdr [32m1207.631[0m  px/s [94m429[0m  prim/s [31m48[0m  rays/s [32m3,434[0m  ints/s [94m197,807,642[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.167316[0m  px_shdr [32m1173.029[0m  px/s [94m442[0m  prim/s [31m49[0m  rays/s [32m3,535[0m  ints/s [94m203,642,650[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.165474[0m  px_shdr [32m1157.915[0m  px/s [94m448[0m  prim/s [31m50[0m  rays/s [32m3,582[0m  ints/s [94m206,300,686[0m  ppm [32m0.004[0m  

gpus_free [33m0.148563[0m
