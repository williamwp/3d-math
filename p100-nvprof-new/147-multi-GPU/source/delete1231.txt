gpus_init [33m4.999853[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m4.700065[0m  px_shdr [32m868.406[0m  px/s [94m597[0m  prim/s [31m66[0m  rays/s [32m4,776[0m  ints/s [94m275,077,273[0m  ppm [32m0.005[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m4.404818[0m  px_shdr [32m710.227[0m  px/s [94m730[0m  prim/s [31m81[0m  rays/s [32m5,839[0m  ints/s [94m336,341,599[0m  ppm [32m0.005[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m3.688825[0m  px_shdr [32m621.149[0m  px/s [94m835[0m  prim/s [31m93[0m  rays/s [32m6,677[0m  ints/s [94m384,575,600[0m  ppm [32m0.005[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m4.190850[0m  px_shdr [32m746.343[0m  px/s [94m695[0m  prim/s [31m77[0m  rays/s [32m5,557[0m  ints/s [94m320,065,679[0m  ppm [32m0.005[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m2.984065[0m  px_shdr [32m659.289[0m  px/s [94m786[0m  prim/s [31m87[0m  rays/s [32m6,290[0m  ints/s [94m362,327,624[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m2.577480[0m  px_shdr [32m650.590[0m  px/s [94m797[0m  prim/s [31m89[0m  rays/s [32m6,375[0m  ints/s [94m367,172,520[0m  ppm [32m0.005[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m2.246492[0m  px_shdr [32m620.647[0m  px/s [94m835[0m  prim/s [31m93[0m  rays/s [32m6,682[0m  ints/s [94m384,886,786[0m  ppm [32m0.005[0m  

gpus_free [33m0.246186[0m
gpus_init [33m3.389414[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m2.359270[0m  px_shdr [32m534.784[0m  px/s [94m969[0m  prim/s [31m108[0m  rays/s [32m7,755[0m  ints/s [94m446,682,746[0m  ppm [32m0.005[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m3.821110[0m  px_shdr [32m586.955[0m  px/s [94m883[0m  prim/s [31m98[0m  rays/s [32m7,066[0m  ints/s [94m406,979,559[0m  ppm [32m0.005[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m3.915146[0m  px_shdr [32m835.141[0m  px/s [94m621[0m  prim/s [31m69[0m  rays/s [32m4,966[0m  ints/s [94m286,033,832[0m  ppm [32m0.006[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m5.525900[0m  px_shdr [32m998.298[0m  px/s [94m519[0m  prim/s [31m58[0m  rays/s [32m4,154[0m  ints/s [94m239,286,079[0m  ppm [32m0.005[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m5.880437[0m  px_shdr [32m1034.808[0m  px/s [94m501[0m  prim/s [31m56[0m  rays/s [32m4,008[0m  ints/s [94m230,843,418[0m  ppm [32m0.006[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m3.588359[0m  px_shdr [32m1047.179[0m  px/s [94m495[0m  prim/s [31m55[0m  rays/s [32m3,960[0m  ints/s [94m228,116,315[0m  ppm [32m0.005[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m3.761090[0m  px_shdr [32m893.504[0m  px/s [94m580[0m  prim/s [31m64[0m  rays/s [32m4,642[0m  ints/s [94m267,350,454[0m  ppm [32m0.005[0m  

gpus_free [33m2.145795[0m
gpus_init [33m6.217826[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m7.222701[0m  px_shdr [32m143.807[0m  px/s [94m3,605[0m  prim/s [31m401[0m  rays/s [32m28,839[0m  ints/s [94m1,661,108,588[0m  ppm [32m0.006[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m9.052278[0m  px_shdr [32m166.006[0m  px/s [94m3,123[0m  prim/s [31m347[0m  rays/s [32m24,982[0m  ints/s [94m1,438,976,611[0m  ppm [32m0.008[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m13.014436[0m  px_shdr [32m221.046[0m  px/s [94m2,345[0m  prim/s [31m261[0m  rays/s [32m18,762[0m  ints/s [94m1,080,675,767[0m  ppm [32m0.007[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m12.729299[0m  px_shdr [32m207.334[0m  px/s [94m2,500[0m  prim/s [31m278[0m  rays/s [32m20,003[0m  ints/s [94m1,152,146,893[0m  ppm [32m0.007[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m11.083323[0m  px_shdr [32m215.132[0m  px/s [94m2,410[0m  prim/s [31m268[0m  rays/s [32m19,277[0m  ints/s [94m1,110,383,417[0m  ppm [32m0.005[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m12.004800[0m  gpus_init [33m10.628852[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m1.827854[0m  px_shdr [32m22.727[0m  px/s [94m22,810[0m  prim/s [31m2,534[0m  rays/s [32m182,481[0m  ints/s [94m10,510,921,341[0m  ppm [32m0.005[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m1.455966[0m  px_shdr [32m23.891[0m  px/s [94m21,698[0m  prim/s [31m2,411[0m  rays/s [32m173,587[0m  ints/s [94m9,998,615,977[0m  ppm [32m0.005[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m1.910422[0m  gpus_init [33m10.045332[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m3.402909[0m  px_shdr [32m26.879[0m  px/s [94m19,286[0m  prim/s [31m2,143[0m  rays/s [32m154,289[0m  ints/s [94m8,887,025,010[0m  ppm [32m0.005[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m3.095162[0m  px_shdr [32m26.648[0m  px/s [94m19,454[0m  prim/s [31m2,162[0m  rays/s [32m155,628[0m  ints/s [94m8,964,195,944[0m  ppm [32m0.005[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m3.046378[0m  px_shdr [32m26.360[0m  px/s [94m19,666[0m  prim/s [31m2,185[0m  rays/s [32m157,331[0m  ints/s [94m9,062,275,978[0m  ppm [32m0.005[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m2.969058[0m  px_shdr [32m27.582[0m  px/s [94m18,795[0m  prim/s [31m2,088[0m  rays/s [32m150,359[0m  ints/s [94m8,660,663,824[0m  ppm [32m0.005[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m2.840360[0m  px_shdr [32m25.490[0m  px/s [94m20,337[0m  prim/s [31m2,260[0m  rays/s [32m162,699[0m  ints/s [94m9,371,484,985[0m  ppm [32m0.005[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m2.840365[0m  px_shdr [32m31.378[0m  px/s [94m16,521[0m  prim/s [31m1,836[0m  rays/s [32m132,168[0m  ints/s [94m7,612,892,401[0m  ppm [32m0.005[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m2.741447[0m  px_shdr [32m27.523[0m  px/s [94m18,835[0m  prim/s [31m2,093[0m  rays/s [32m150,679[0m  ints/s [94m8,679,138,315[0m  ppm [32m0.005[0m  

gpus_free [33m4.021181[0m
gpus_init [33m8.953252[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m3.139032[0m  px_shdr [32m11.125[0m  px/s [94m46,597[0m  prim/s [31m5,177[0m  rays/s [32m372,772[0m  ints/s [94m21,471,693,588[0m  ppm [32m0.008[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m2.183141[0m  px_shdr [32m11.509[0m  px/s [94m45,043[0m  prim/s [31m5,005[0m  rays/s [32m360,341[0m  ints/s [94m20,755,650,322[0m  ppm [32m0.008[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m2.741219[0m  px_shdr [32m11.056[0m  px/s [94m46,890[0m  prim/s [31m5,210[0m  rays/s [32m375,117[0m  ints/s [94m21,606,764,546[0m  ppm [32m0.007[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m2.696998[0m  px_shdr [32m11.426[0m  px/s [94m45,369[0m  prim/s [31m5,041[0m  rays/s [32m362,952[0m  ints/s [94m20,906,030,335[0m  ppm [32m0.008[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m2.509101[0m  px_shdr [32m20.663[0m  px/s [94m25,089[0m  prim/s [31m2,788[0m  rays/s [32m200,708[0m  ints/s [94m11,560,799,674[0m  ppm [32m0.006[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m2.549451[0m  px_shdr [32m11.074[0m  px/s [94m46,811[0m  prim/s [31m5,201[0m  rays/s [32m374,487[0m  ints/s [94m21,570,431,045[0m  ppm [32m0.007[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m2.648906[0m  px_shdr [32m11.136[0m  px/s [94m46,552[0m  prim/s [31m5,172[0m  rays/s [32m372,413[0m  ints/s [94m21,450,966,678[0m  ppm [32m0.006[0m  

gpus_free [33m4.027374[0m
gpus_init [33m8.808949[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m9.812273[0m  px_shdr [32m14.632[0m  px/s [94m35,429[0m  prim/s [31m3,937[0m  rays/s [32m283,434[0m  ints/s [94m16,325,779,011[0m  ppm [32m0.007[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m8.684688[0m  px_shdr [32m22.810[0m  px/s [94m22,727[0m  prim/s [31m2,525[0m  rays/s [32m181,815[0m  ints/s [94m10,472,531,246[0m  ppm [32m0.008[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m11.324169[0m  px_shdr [32m13.706[0m  px/s [94m37,822[0m  prim/s [31m4,202[0m  rays/s [32m302,575[0m  ints/s [94m17,428,321,130[0m  ppm [32m0.007[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m8.273221[0m  px_shdr [32m13.776[0m  px/s [94m37,631[0m  prim/s [31m4,181[0m  rays/s [32m301,045[0m  ints/s [94m17,340,196,179[0m  ppm [32m0.006[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m8.177385[0m  px_shdr [32m13.774[0m  px/s [94m37,635[0m  prim/s [31m4,182[0m  rays/s [32m301,081[0m  ints/s [94m17,342,292,827[0m  ppm [32m0.006[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m9.093601[0m  px_shdr [32m22.908[0m  px/s [94m22,630[0m  prim/s [31m2,514[0m  rays/s [32m181,040[0m  ints/s [94m10,427,897,508[0m  ppm [32m0.007[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m9.578580[0m  px_shdr [32m14.425[0m  px/s [94m35,937[0m  prim/s [31m3,993[0m  rays/s [32m287,497[0m  ints/s [94m16,559,823,580[0m  ppm [32m0.007[0m  

gpus_free [33m3.964955[0m
gpus_init [33m8.289223[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m25.113512[0m  px_shdr [32m168.022[0m  px/s [94m3,085[0m  prim/s [31m343[0m  rays/s [32m24,682[0m  ints/s [94m1,421,707,838[0m  ppm [32m0.006[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m22.987028[0m  gpus_init [33m8.128155[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m23.510789[0m  px_shdr [32m173.773[0m  px/s [94m2,983[0m  prim/s [31m331[0m  rays/s [32m23,866[0m  ints/s [94m1,374,657,886[0m  ppm [32m0.006[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m22.667493[0m  px_shdr [32m159.901[0m  px/s [94m3,242[0m  prim/s [31m360[0m  rays/s [32m25,936[0m  ints/s [94m1,493,917,596[0m  ppm [32m0.008[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m17.602642[0m  px_shdr [32m131.131[0m  px/s [94m3,953[0m  prim/s [31m439[0m  rays/s [32m31,626[0m  ints/s [94m1,821,677,677[0m  ppm [32m0.006[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m21.918143[0m  px_shdr [32m144.221[0m  px/s [94m3,594[0m  prim/s [31m399[0m  rays/s [32m28,756[0m  ints/s [94m1,656,332,461[0m  ppm [32m0.005[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m22.145619[0m  px_shdr [32m130.568[0m  px/s [94m3,970[0m  prim/s [31m441[0m  rays/s [32m31,763[0m  ints/s [94m1,829,537,148[0m  ppm [32m0.005[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m23.739485[0m  px_shdr [32m121.912[0m  px/s [94m4,252[0m  prim/s [31m472[0m  rays/s [32m34,018[0m  ints/s [94m1,959,433,294[0m  ppm [32m0.008[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m18.774891[0m  px_shdr [32m128.471[0m  px/s [94m4,035[0m  prim/s [31m448[0m  rays/s [32m32,281[0m  ints/s [94m1,859,402,273[0m  ppm [32m0.007[0m  

gpus_free [33m2.919796[0m
gpus_init [33m8.583240[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m27.973733[0m  px_shdr [32m195.946[0m  px/s [94m2,646[0m  prim/s [31m294[0m  rays/s [32m21,165[0m  ints/s [94m1,219,104,199[0m  ppm [32m0.007[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m33.363852[0m  px_shdr [32m186.103[0m  px/s [94m2,786[0m  prim/s [31m310[0m  rays/s [32m22,284[0m  ints/s [94m1,283,584,682[0m  ppm [32m0.005[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m31.870807[0m  px_shdr [32m173.604[0m  px/s [94m2,986[0m  prim/s [31m332[0m  rays/s [32m23,889[0m  ints/s [94m1,376,001,614[0m  ppm [32m0.007[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m33.411010[0m  px_shdr [32m184.849[0m  px/s [94m2,804[0m  prim/s [31m312[0m  rays/s [32m22,436[0m  ints/s [94m1,292,291,534[0m  ppm [32m0.007[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m31.746421[0m  px_shdr [32m334.494[0m  px/s [94m1,550[0m  prim/s [31m172[0m  rays/s [32m12,398[0m  ints/s [94m714,149,701[0m  ppm [32m0.005[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m7.631025[0m  px_shdr [32m263.460[0m  px/s [94m1,968[0m  prim/s [31m219[0m  rays/s [32m15,741[0m  ints/s [94m906,697,040[0m  ppm [32m0.005[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m11.370628[0m  