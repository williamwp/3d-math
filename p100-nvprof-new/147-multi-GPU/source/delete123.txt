gpus_init [33m4.716391[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m3.457403[0m  px_shdr [32m245.342[0m  px/s [94m2,113[0m  prim/s [31m235[0m  rays/s [32m16,904[0m  ints/s [94m973,656,191[0m  ppm [32m0.004[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m3.155453[0m  px_shdr [32m245.384[0m  px/s [94m2,113[0m  prim/s [31m235[0m  rays/s [32m16,901[0m  ints/s [94m973,488,434[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m2.989939[0m  px_shdr [32m241.925[0m  px/s [94m2,143[0m  prim/s [31m238[0m  rays/s [32m17,143[0m  ints/s [94m987,409,738[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m3.128963[0m  px_shdr [32m244.665[0m  px/s [94m2,119[0m  prim/s [31m235[0m  rays/s [32m16,950[0m  ints/s [94m976,348,633[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m3.047463[0m  px_shdr [32m253.424[0m  px/s [94m2,046[0m  prim/s [31m227[0m  rays/s [32m16,365[0m  ints/s [94m942,604,290[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m3.064338[0m  px_shdr [32m257.611[0m  px/s [94m2,012[0m  prim/s [31m224[0m  rays/s [32m16,099[0m  ints/s [94m927,284,074[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m2.958519[0m  px_shdr [32m254.491[0m  px/s [94m2,037[0m  prim/s [31m226[0m  rays/s [32m16,296[0m  ints/s [94m938,653,207[0m  ppm [32m0.004[0m  

gpus_free [33m0.161944[0m
gpus_init [33m4.802046[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m3.174682[0m  px_shdr [32m232.790[0m  px/s [94m2,227[0m  prim/s [31m247[0m  rays/s [32m17,815[0m  ints/s [94m1,026,154,472[0m  ppm [32m0.005[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m3.070661[0m  px_shdr [32m238.691[0m  px/s [94m2,172[0m  prim/s [31m241[0m  rays/s [32m17,375[0m  ints/s [94m1,000,787,090[0m  ppm [32m0.005[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m2.846369[0m  px_shdr [32m241.480[0m  px/s [94m2,147[0m  prim/s [31m239[0m  rays/s [32m17,174[0m  ints/s [94m989,229,319[0m  ppm [32m0.005[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m2.555337[0m  px_shdr [32m226.746[0m  px/s [94m2,286[0m  prim/s [31m254[0m  rays/s [32m18,290[0m  ints/s [94m1,053,509,258[0m  ppm [32m0.005[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m2.832622[0m  px_shdr [32m233.710[0m  px/s [94m2,218[0m  prim/s [31m246[0m  rays/s [32m17,745[0m  ints/s [94m1,022,118,041[0m  ppm [32m0.005[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m2.947700[0m  px_shdr [32m226.566[0m  px/s [94m2,288[0m  prim/s [31m254[0m  rays/s [32m18,305[0m  ints/s [94m1,054,347,192[0m  ppm [32m0.005[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m2.920576[0m  px_shdr [32m234.032[0m  px/s [94m2,215[0m  prim/s [31m246[0m  rays/s [32m17,721[0m  ints/s [94m1,020,710,305[0m  ppm [32m0.005[0m  

gpus_free [33m0.224821[0m
gpus_init [33m36.640810[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m35.388823[0m  px_shdr [32m296.239[0m  px/s [94m1,750[0m  prim/s [31m194[0m  rays/s [32m13,999[0m  ints/s [94m806,370,517[0m  ppm [32m0.023[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m3.042789[0m  px_shdr [32m196.963[0m  px/s [94m2,632[0m  prim/s [31m292[0m  rays/s [32m21,056[0m  ints/s [94m1,212,811,203[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m2.968520[0m  px_shdr [32m187.766[0m  px/s [94m2,761[0m  prim/s [31m307[0m  rays/s [32m22,087[0m  ints/s [94m1,272,214,341[0m  ppm [32m0.005[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m3.188343[0m  px_shdr [32m202.709[0m  px/s [94m2,557[0m  prim/s [31m284[0m  rays/s [32m20,459[0m  ints/s [94m1,178,429,645[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m2.887515[0m  px_shdr [32m194.061[0m  px/s [94m2,671[0m  prim/s [31m297[0m  rays/s [32m21,371[0m  ints/s [94m1,230,946,854[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m2.905071[0m  px_shdr [32m211.209[0m  px/s [94m2,454[0m  prim/s [31m273[0m  rays/s [32m19,636[0m  ints/s [94m1,131,007,254[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m2.809265[0m  px_shdr [32m217.353[0m  px/s [94m2,385[0m  prim/s [31m265[0m  rays/s [32m19,080[0m  ints/s [94m1,099,034,005[0m  ppm [32m0.005[0m  

gpus_free [33m0.286908[0m
gpus_init [33m2.362525[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m0.461973[0m  px_shdr [32m114.662[0m  px/s [94m4,521[0m  prim/s [31m502[0m  rays/s [32m36,169[0m  ints/s [94m2,083,333,870[0m  ppm [32m0.004[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.268213[0m  px_shdr [32m110.241[0m  px/s [94m4,702[0m  prim/s [31m522[0m  rays/s [32m37,619[0m  ints/s [94m2,166,880,290[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.305571[0m  px_shdr [32m114.411[0m  px/s [94m4,531[0m  prim/s [31m503[0m  rays/s [32m36,248[0m  ints/s [94m2,087,908,250[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.247235[0m  px_shdr [32m105.177[0m  px/s [94m4,929[0m  prim/s [31m548[0m  rays/s [32m39,431[0m  ints/s [94m2,271,205,840[0m  ppm [32m0.004[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.253450[0m  px_shdr [32m110.667[0m  px/s [94m4,684[0m  prim/s [31m520[0m  rays/s [32m37,475[0m  ints/s [94m2,158,535,005[0m  ppm [32m0.008[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.230570[0m  px_shdr [32m114.848[0m  px/s [94m4,514[0m  prim/s [31m502[0m  rays/s [32m36,110[0m  ints/s [94m2,079,961,015[0m  ppm [32m0.005[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.281738[0m  px_shdr [32m102.093[0m  px/s [94m5,078[0m  prim/s [31m564[0m  rays/s [32m40,622[0m  ints/s [94m2,339,818,175[0m  ppm [32m0.004[0m  

gpus_free [33m0.166587[0m
gpus_init [33m4.279468[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m3.027911[0m  px_shdr [32m221.918[0m  px/s [94m2,336[0m  prim/s [31m260[0m  rays/s [32m18,688[0m  ints/s [94m1,076,426,564[0m  ppm [32m0.004[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m2.625998[0m  px_shdr [32m214.353[0m  px/s [94m2,418[0m  prim/s [31m269[0m  rays/s [32m19,348[0m  ints/s [94m1,114,419,765[0m  ppm [32m0.004[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m2.724735[0m  px_shdr [32m195.701[0m  px/s [94m2,649[0m  prim/s [31m294[0m  rays/s [32m21,192[0m  ints/s [94m1,220,631,159[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m2.693449[0m  px_shdr [32m207.627[0m  px/s [94m2,497[0m  prim/s [31m277[0m  rays/s [32m19,974[0m  ints/s [94m1,150,520,601[0m  ppm [32m0.005[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m2.656746[0m  px_shdr [32m195.489[0m  px/s [94m2,652[0m  prim/s [31m295[0m  rays/s [32m21,214[0m  ints/s [94m1,221,954,923[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m2.794209[0m  px_shdr [32m198.167[0m  px/s [94m2,616[0m  prim/s [31m291[0m  rays/s [32m20,928[0m  ints/s [94m1,205,439,745[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m2.743757[0m  px_shdr [32m184.831[0m  px/s [94m2,805[0m  prim/s [31m312[0m  rays/s [32m22,438[0m  ints/s [94m1,292,419,747[0m  ppm [32m0.005[0m  

gpus_free [33m0.159945[0m
gpus_init [33m5.488385[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m4.651047[0m  px_shdr [32m276.388[0m  px/s [94m1,876[0m  prim/s [31m208[0m  rays/s [32m15,005[0m  ints/s [94m864,288,113[0m  ppm [32m0.005[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m4.197278[0m  