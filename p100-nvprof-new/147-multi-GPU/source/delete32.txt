gpus_init [33m3.320088[0m

[94mimg_w[0m     [0m960[0m
[35mimg_h[0m     [0m540[0m
[31mnframes[0m   [0m7[0m
[32mnsamples[0m  [0m2[0m
[94mnbounces[0m  [0m4[0m

[32mimg[0m dir                   [33m.[0m
[32mtriangles[0m nelems          [94m57,600[0m
[32mtriangles[0m nbytes          [35m2,534,400[0m

[32mnintersections[0m any frame  [94m238,878,720,000[0m
[32mnintersections[0m all frames [35m1,672,151,040,000[0m

[35m0000[0m [31m 0.000[0m  mesh_shdr [32m1.223628[0m  px_shdr [32m4.860[0m  px/s [94m106,657[0m  prim/s [31m11,851[0m  rays/s [32m853,257[0m  ints/s [94m49,147,581,612[0m  ppm [32m0.005[0m  
[35m0001[0m [31m 2.000[0m  mesh_shdr [32m0.600116[0m  px_shdr [32m4.747[0m  px/s [94m109,210[0m  prim/s [31m12,134[0m  rays/s [32m873,676[0m  ints/s [94m50,323,738,857[0m  ppm [32m0.005[0m  
[35m0002[0m [31m 4.000[0m  mesh_shdr [32m0.289068[0m  px_shdr [32m4.501[0m  px/s [94m115,183[0m  prim/s [31m12,798[0m  rays/s [32m921,464[0m  ints/s [94m53,076,339,910[0m  ppm [32m0.004[0m  
[35m0003[0m [31m 6.000[0m  mesh_shdr [32m0.207828[0m  px_shdr [32m4.628[0m  px/s [94m112,022[0m  prim/s [31m12,447[0m  rays/s [32m896,172[0m  ints/s [94m51,619,526,761[0m  ppm [32m0.005[0m  
[35m0004[0m [31m 8.000[0m  mesh_shdr [32m0.262810[0m  px_shdr [32m4.499[0m  px/s [94m115,226[0m  prim/s [31m12,803[0m  rays/s [32m921,806[0m  ints/s [94m53,096,014,759[0m  ppm [32m0.004[0m  
[35m0005[0m [31m10.000[0m  mesh_shdr [32m0.200961[0m  px_shdr [32m4.531[0m  px/s [94m114,422[0m  prim/s [31m12,714[0m  rays/s [32m915,378[0m  ints/s [94m52,725,798,704[0m  ppm [32m0.004[0m  
[35m0006[0m [31m12.000[0m  mesh_shdr [32m0.293968[0m  px_shdr [32m4.530[0m  px/s [94m114,427[0m  prim/s [31m12,714[0m  rays/s [32m915,419[0m  ints/s [94m52,728,149,739[0m  ppm [32m0.004[0m  

gpus_free [33m0.432017[0m
Cost time is 0 min 40 s
