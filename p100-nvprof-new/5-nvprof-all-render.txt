==15861== Profiling application: ./5-maine00
==15861== Profiling result:
==15861== Metric result:
Invocations                               Metric Name                                    Metric Description         Min         Max         Avg
Device "Tesla P100-PCIE-16GB (0)"
    Kernel: render(vec3*, int, int, hitable_list**, camera**, curandStateXORWOW*)
          1                             inst_per_warp                                 Instructions per warp  1.2947e+06  1.2947e+06  1.2947e+06
          1                         branch_efficiency                                     Branch Efficiency      98.33%      98.33%      98.33%
          1                 warp_execution_efficiency                             Warp Execution Efficiency      25.95%      25.95%      25.95%
          1         warp_nonpred_execution_efficiency              Warp Non-Predicated Execution Efficiency      25.43%      25.43%      25.43%
          1                      inst_replay_overhead                           Instruction Replay Overhead    0.000062    0.000062    0.000062
          1      shared_load_transactions_per_request           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
          1     shared_store_transactions_per_request          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
          1       local_load_transactions_per_request            Local Memory Load Transactions Per Request    3.251291    3.251291    3.251291
          1      local_store_transactions_per_request           Local Memory Store Transactions Per Request    2.039511    2.039511    2.039511
          1              gld_transactions_per_request                  Global Load Transactions Per Request    6.714728    6.714728    6.714728
          1              gst_transactions_per_request                 Global Store Transactions Per Request    0.112197    0.112197    0.112197
          1                 shared_store_transactions                             Shared Store Transactions           0           0           0
          1                  shared_load_transactions                              Shared Load Transactions           0           0           0
          1                   local_load_transactions                               Local Load Transactions  1.2885e+10  1.2885e+10  1.2885e+10
          1                  local_store_transactions                              Local Store Transactions  6773582414  6773582414  6773582414
          1                          gld_transactions                              Global Load Transactions  1.8406e+10  1.8406e+10  1.8406e+10
          1                          gst_transactions                             Global Store Transactions     1080000     1080000     1080000
          1                  sysmem_read_transactions                       System Memory Read Transactions           0           0           0
          1                 sysmem_write_transactions                      System Memory Write Transactions           5           5           5
          1                      l2_read_transactions                                  L2 Read Transactions  8113946186  8113946186  8113946186
          1                     l2_write_transactions                                 L2 Write Transactions  8937772057  8937772057  8937772057
          1                    dram_read_transactions                       Device Memory Read Transactions  6482734425  6482734425  6482734425
          1                   dram_write_transactions                      Device Memory Write Transactions  5632777156  5632777156  5632777156
          1                           global_hit_rate                     Global Hit Rate in unified l1/tex      78.73%      78.73%      78.73%
          1                            local_hit_rate                                        Local Hit Rate      42.67%      42.67%      42.67%
          1                  gld_requested_throughput                      Requested Global Load Throughput  7.3983GB/s  7.3983GB/s  7.3983GB/s
          1                  gst_requested_throughput                     Requested Global Store Throughput  49.992MB/s  49.992MB/s  49.992MB/s
          1                            gld_throughput                                Global Load Throughput  63.101GB/s  63.101GB/s  63.101GB/s
          1                            gst_throughput                               Global Store Throughput  20.430MB/s  20.430MB/s  20.430MB/s
          1                     local_memory_overhead                                 Local Memory Overhead      95.12%      95.12%      95.12%
          1                        tex_cache_hit_rate                                Unified Cache Hit Rate      49.95%      49.95%      49.95%
          1                      l2_tex_read_hit_rate                           L2 Hit Rate (Texture Reads)      41.76%      41.76%      41.76%
          1                     l2_tex_write_hit_rate                          L2 Hit Rate (Texture Writes)      34.32%      34.32%      34.32%
          1                      dram_read_throughput                         Device Memory Read Throughput  119.76GB/s  119.76GB/s  119.76GB/s
          1                     dram_write_throughput                        Device Memory Write Throughput  104.06GB/s  104.06GB/s  104.06GB/s
          1                      tex_cache_throughput                              Unified Cache Throughput  241.37GB/s  241.37GB/s  241.37GB/s
          1                    l2_tex_read_throughput                         L2 Throughput (Texture Reads)  149.89GB/s  149.89GB/s  149.89GB/s
          1                   l2_tex_write_throughput                        L2 Throughput (Texture Writes)  125.15GB/s  125.15GB/s  125.15GB/s
          1                        l2_read_throughput                                 L2 Throughput (Reads)  149.89GB/s  149.89GB/s  149.89GB/s
          1                       l2_write_throughput                                L2 Throughput (Writes)  165.11GB/s  165.11GB/s  165.11GB/s
          1                    sysmem_read_throughput                         System Memory Read Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          1                   sysmem_write_throughput                        System Memory Write Throughput  99.0000B/s  99.0000B/s  98.0000B/s
          1                     local_load_throughput                          Local Memory Load Throughput  238.02GB/s  238.02GB/s  238.02GB/s
          1                    local_store_throughput                         Local Memory Store Throughput  125.13GB/s  125.13GB/s  125.13GB/s
          1                    shared_load_throughput                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          1                   shared_store_throughput                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          1                            gld_efficiency                         Global Memory Load Efficiency      11.72%      11.72%      11.72%
          1                            gst_efficiency                        Global Memory Store Efficiency     244.70%     244.70%     244.70%
          1                    tex_cache_transactions                            Unified Cache Transactions  1.3066e+10  1.3066e+10  1.3066e+10
          1                             flop_count_dp           Floating Point Operations(Double Precision)  2824220551  2824220551  2824220551
          1                         flop_count_dp_add       Floating Point Operations(Double Precision Add)   345406025   345406025   345406025
          1                         flop_count_dp_fma       Floating Point Operations(Double Precision FMA)  1126733840  1126733840  1126733840
          1                         flop_count_dp_mul       Floating Point Operations(Double Precision Mul)   225346846   225346846   225346846
          1                             flop_count_sp           Floating Point Operations(Single Precision)  1.1105e+11  1.1105e+11  1.1105e+11
          1                         flop_count_sp_add       Floating Point Operations(Single Precision Add)  3.1863e+10  3.1863e+10  3.1863e+10
          1                         flop_count_sp_fma       Floating Point Operations(Single Precision FMA)  2.9367e+10  2.9367e+10  2.9367e+10
          1                         flop_count_sp_mul        Floating Point Operation(Single Precision Mul)  2.0457e+10  2.0457e+10  2.0457e+10
          1                     flop_count_sp_special   Floating Point Operations(Single Precision Special)  4472531345  4472531345  4472531345
          1                             inst_executed                                 Instructions Executed  5.8845e+10  5.8845e+10  5.8845e+10
          1                               inst_issued                                   Instructions Issued  5.8849e+10  5.8849e+10  5.8849e+10
          1                          dram_utilization                             Device Memory Utilization     Mid (4)     Mid (4)     Mid (4)
          1                        sysmem_utilization                             System Memory Utilization     Low (1)     Low (1)     Low (1)
          1                          stall_inst_fetch              Issue Stall Reasons (Instructions Fetch)      10.52%      10.52%      10.52%
          1                     stall_exec_dependency            Issue Stall Reasons (Execution Dependency)       4.84%       4.84%       4.84%
          1                   stall_memory_dependency                    Issue Stall Reasons (Data Request)      79.50%      79.50%      79.50%
          1                             stall_texture                         Issue Stall Reasons (Texture)       0.24%       0.24%       0.24%
          1                                stall_sync                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
          1                               stall_other                           Issue Stall Reasons (Other)       4.16%       4.16%       4.16%
          1          stall_constant_memory_dependency              Issue Stall Reasons (Immediate constant)       0.00%       0.00%       0.00%
          1                           stall_pipe_busy                       Issue Stall Reasons (Pipe Busy)       0.27%       0.27%       0.27%
          1                         shared_efficiency                              Shared Memory Efficiency       0.00%       0.00%       0.00%
          1                                inst_fp_32                               FP Instructions(Single)  1.0385e+11  1.0385e+11  1.0385e+11
          1                                inst_fp_64                               FP Instructions(Double)  1697486789  1697486789  1697486789
          1                              inst_integer                                  Integer Instructions  1.6740e+11  1.6740e+11  1.6740e+11
          1                          inst_bit_convert                              Bit-Convert Instructions  3314962491  3314962491  3314962491
          1                              inst_control                             Control-Flow Instructions  4.6770e+10  4.6770e+10  4.6770e+10
          1                        inst_compute_ld_st                               Load/Store Instructions  8.9352e+10  8.9352e+10  8.9352e+10
          1                                 inst_misc                                     Misc Instructions  6.6457e+10  6.6457e+10  6.6457e+10
          1           inst_inter_thread_communication                             Inter-Thread Instructions           0           0           0
          1                               issue_slots                                           Issue Slots  5.2726e+10  5.2726e+10  5.2726e+10
          1                                 cf_issued                      Issued Control-Flow Instructions  6896138195  6896138195  6896138195
          1                               cf_executed                    Executed Control-Flow Instructions  6896138195  6896138195  6896138195
          1                               ldst_issued                        Issued Load/Store Instructions  1.9129e+10  1.9129e+10  1.9129e+10
          1                             ldst_executed                      Executed Load/Store Instructions  1.0372e+10  1.0372e+10  1.0372e+10
          1                       atomic_transactions                                   Atomic Transactions           0           0           0
          1           atomic_transactions_per_request                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
          1                      l2_atomic_throughput                       L2 Throughput (Atomic requests)  0.00000B/s  0.00000B/s  0.00000B/s
          1                    l2_atomic_transactions                     L2 Transactions (Atomic requests)           0           0           0
          1                     stall_memory_throttle                 Issue Stall Reasons (Memory Throttle)       0.00%       0.00%       0.00%
          1                        stall_not_selected                    Issue Stall Reasons (Not Selected)       0.46%       0.46%       0.46%
          1                 l2_tex_write_transactions                      L2 Transactions (Texture Writes)  6774662414  6774662414  6774662414
          1                             flop_count_hp             Floating Point Operations(Half Precision)           0           0           0
          1                         flop_count_hp_add         Floating Point Operations(Half Precision Add)           0           0           0
          1                         flop_count_hp_mul          Floating Point Operation(Half Precision Mul)           0           0           0
          1                         flop_count_hp_fma         Floating Point Operations(Half Precision FMA)           0           0           0
          1                                inst_fp_16                                 HP Instructions(Half)           0           0           0
          1                                       ipc                                                          Executed IPC    0.500316    0.500316    0.500316
          1                                issued_ipc                                                            Issued IPC    0.499891    0.499891    0.499891
          1                    issue_slot_utilization                                                Issue Slot Utilization      22.39%      22.39%      22.39%
          1                             sm_efficiency                                               Multiprocessor Activity      98.26%      98.26%      98.26%
          1                        achieved_occupancy                                                    Achieved Occupancy    0.486536    0.486536    0.486536
          1                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    0.587816    0.587816    0.587816
          1                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
          1                            l2_utilization                                                  L2 Cache Utilization     Low (2)     Low (2)     Low (2)
          1                           tex_utilization                                             Unified Cache Utilization     Low (2)     Low (2)     Low (2)
          1                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1                        tex_fu_utilization                                     Texture Function Unit Utilization     Low (2)     Low (2)     Low (2)
          1                    special_fu_utilization                                     Special Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
          1           single_precision_fu_utilization                            Single-Precision Function Unit Utilization     Low (2)     Low (2)     Low (2)
          1           double_precision_fu_utilization                            Double-Precision Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
          1                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)       0.72%       0.72%       0.72%
          1                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)       0.04%       0.04%       0.04%
          1                   sysmem_read_utilization                                        System Memory Read Utilization    Idle (0)    Idle (0)    Idle (0)
          1                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
          1               pcie_total_data_transmitted                                           PCIe Total Data Transmitted      771072      771072      771072
          1                  pcie_total_data_received                                              PCIe Total Data Received      198656      198656      198656
          1                inst_executed_global_loads                              Warp level instructions for global loads  2741166303  2741166303  2741166303
          1                 inst_executed_local_loads                               Warp level instructions for local loads  3962909407  3962909407  3962909407
          1                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
          1               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
          1               inst_executed_global_stores                             Warp level instructions for global stores     9625898     9625898     9625898
          1                inst_executed_local_stores                              Warp level instructions for local stores  3321179205  3321179205  3321179205
          1               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
          1              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
          1              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
          1           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
          1             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
          1          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
          1              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
          1                     inst_executed_tex_ops                                   Warp level instructions for texture           0           0           0
          1                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads  2.3243e+10  2.3243e+10  2.3243e+10
          1                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads  2.3640e+11  2.3640e+11  2.3640e+11
          1                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
          1                           dram_read_bytes                                Total bytes read from DRAM to L2 cache  2.0746e+11  2.0746e+11  2.0746e+11
          1                          dram_write_bytes                             Total bytes written from L2 cache to DRAM  1.8026e+11  1.8026e+11  1.8026e+11
          1               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.  2.1679e+11  2.1679e+11  2.1679e+11
          1                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0           0           0
          1              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0           0           0
          1                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
          1                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
          1             l2_surface_atomic_store_bytes   Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
          1                      global_load_requests             Total number of global load requests from Multiprocessor  4541954107  4541954107  4541954107
          1                       local_load_requests              Total number of local load requests from Multiprocessor  7610214982  7610214982  7610214982
          1                     surface_load_requests            Total number of surface load requests from Multiprocessor           0           0           0
          1                     global_store_requests            Total number of global store requests from Multiprocessor      270000      270000      270000
          1                      local_store_requests             Total number of local store requests from Multiprocessor  6683583691  6683583691  6683583691
          1                    surface_store_requests           Total number of surface store requests from Multiprocessor           0           0           0
          1                    global_atomic_requests           Total number of global atomic requests from Multiprocessor           0           0           0
          1                 global_reduction_requests        Total number of global reduction requests from Multiprocessor           0           0           0
          1                   surface_atomic_requests          Total number of surface atomic requests from Multiprocessor           0           0           0
          1                surface_reduction_requests       Total number of surface reduction requests from Multiprocessor           0           0           0
          1                         sysmem_read_bytes                                             System Memory Read Bytes           0           0           0
          1                        sysmem_write_bytes                                            System Memory Write Bytes         160         160         160
          1                           l2_tex_hit_rate                                                    L2 Cache Hit Rate      38.36%      38.36%      38.36%
          1                     texture_load_requests            Total number of texture Load requests from Multiprocessor           0           0           0
          1                     unique_warps_launched                                             Number of warps launched       45451       45451       45451


